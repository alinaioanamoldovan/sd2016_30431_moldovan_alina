
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

input[type="text"], textarea {

 background-image: url('http://crunchify.com/bg.png');

}

.nothing {
    border: 0;
    outline: none;
}
</style>
<body>


	 <ul>
	  <li><a href="http://localhost:8080/spring/homeAdmin">Home</a></li>
	  <li><a href="http://localhost:8080/spring/addBook">Add Book</a></li>
	  <li><a href="http://localhost:8080/spring/viewBook">View Books</a></li>
	  <li><a href="http://localhost:8080/spring/addEmployee">Add Employee</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	
	<h2>About </h2>
	<h3> Account information </h3>
	<form name="editForm"  method = 'Post'>
	<table >
	<tr>
	   <td>Username:</td>  
	   <td>${username}</td>
	</tr>
	<tr>
	   <td>User Role:</td>  
	   <td>${role} </td>
	</tr>
	 
	</table>
	<h3> Basic information</h3>
	<table>
	<tr>
		<td>First Name:</td> 
	     <td> <input class="nothing" type='text' th:field="*{firstName}" name='firstName' value='${firstName}'/> </td>
     <tr>
	<tr>
	     <td>Last  Name:</td>
	     <td>   <input class="nothing" type='text' th:field="*{lastName}" name='lastName' value='${lastName}'/></td>
	</tr>
	<tr>
		<td>Email:</td>
		<td>   <input class="nothing" type='text' th:field="*{email}" name='email' value='${email}'/></td>
	<tr>
	</table>
	<input type="submit" name="submit" value="submit" class="button"/>		
	
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				
</form>	

</body>
</html>