<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/views/template/header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
<head><title>Register</title></head>
<body>
 <h1>Register</h1>
 <form name='registerForm'  th:object="${register}" method='POST'>
    <table>
       <tr>
          <td>User:</td>
          <td><input type='text' th:field="*{username}" name='username' value=''></td>
       </tr>
       <tr>
          <td>Email:</td>
          <td><input type='text' th:field="*{email}" name='email' value=''></td>
       </tr>
       <tr>
          <td>Password:</td>
          <td><input type='password' th:field="*{password}" name='password' /></td>
       </tr>
       <tr>
          <td>Repeat Password:</td>
          <td><input type='password' th:field="*{repeatedpass}" name='repeatedpass' /></td>
       </tr>
       
       <tr>
          <td><input name="submit" type="submit" value="submit" /></td>
       </tr>
    </table>
    <font color="red">${message2}</font>
</form>
</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>