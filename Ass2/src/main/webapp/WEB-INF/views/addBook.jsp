<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}




</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Book</title>
</head>
<body>
	 <ul>
	  <li><a href="http://localhost:8080/spring/homeAdmin">Home</a></li>
	  <li><a href="http://localhost:8080/spring/addBook">Add Book</a></li>
	  <li><a href="http://localhost:8080/spring/viewBook">View Books</a></li>
	  <li><a href="http://localhost:8080/spring/addEmployee">Add Employee</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	<div align="center">
 <h1>Add Book </h1>
 <form name='registerForm'  th:object="${book}" method='POST'>
    <table>
    	<tr>
          <td>Title:</td>
          <td><input type='text' th:field="*{title}" name='title' value=''></td>
       </tr>
       <tr>
          <td>Author:</td>
          <td><input type='text' th:field="*{author}" name='author' value=''></td>
       </tr>
       
       <tr>
          <td>Genre:</td>
          <td><input type='text' th:field="*{genre}" name='genre' value=''></td>
       </tr>
       <tr>
          <td>Description:</td>
          <td><input type='text' th:field="*{description}" name='description' value=''></td>
       </tr>
       <tr>
       		 <tr>
          <td>Quantity:</td>
          <td><input type='text' th:field="*{quantity}" name='quantity' /></td>
       </tr>
		  
		  </select>
       </tr>
       <tr>
          <td>Price:</td>
          <td><input type='text' th:field="*{price}" name='price' /></td>
       </tr>
       
         <tr>
          <td>Code:</td>
          <td><input type='text' th:field="*{code}" name='code' /></td>
       </tr>
       
       
       <tr>
          <td><input name="submit" type="submit" value="submit" /></td>
       </tr>
       
      
    </table>
     <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
       
    <font color="red">${message2}</font>
</form>
</div>

</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>