
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}





</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Employee</title>
</head>
<body>
	<ul>
	  <li><a href="http://localhost:8080/spring/homeAdmin">Home</a></li>
	  <li><a href="http://localhost:8080/spring/addBook">Add Book</a></li>
	  <li><a href="http://localhost:8080/spring/viewBook">View Books</a></li>
	  <li><a href="http://localhost:8080/spring/addEmployee">Add Employee</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>

	<div align="center">
 <h1>Add Employee</h1>
 <form name='registerForm'  th:object="${employee}" method='POST'>
    <table>
    	<tr>
          <td>First Name:</td>
          <td><input type='text' th:field="*{firstName}" name='firstName' value=''></td>
       </tr>
       <tr>
          <td>Last Name:</td>
          <td><input type='text' th:field="*{lastName}" name='lastName' value=''></td>
       </tr>
       
       <tr>
          <td>Username:</td>
          <td><input type='text' th:field="*{username}" name='username' value=''></td>
       </tr>
       <tr>
          <td>Email:</td>
          <td><input type='text' th:field="*{email}" name='email' value=''></td>
       </tr>
       <tr>
       		<td>Role:</td>
       		<td><select th:field="*{role}" name='role'>
       		<option value="REGULAR_USER">Regular Employee</option>
		   <option value="ADMIN">Admin</option>
		  
		  </select>
       </tr>
       <tr>
          <td>Password:</td>
          <td><input type='password' th:field="*{password}" name='password' /></td>
       </tr>
       
       
       <tr>
          <td><input name="submit" type="submit" value="submit" /></td>
       </tr>
       
      
    </table>
     <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
       
    <font color="red">${message2}</font>
</form>
</div>




</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>