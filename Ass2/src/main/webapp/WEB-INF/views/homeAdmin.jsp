<%@ include file="/WEB-INF/views/template/header.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 15px;
    text-align: left;
}



</style>


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HomeAdmin</title>
    </head>
    <body>
    <ul>
	  <li><a href="http://localhost:8080/spring/homeAdmin">Home</a></li>
	  <li><a href="http://localhost:8080/spring/addBook">Add Book</a></li>
	  <li><a href="http://localhost:8080/spring/viewBook">View Books</a></li>
	  <li><a href="http://localhost:8080/spring/deleteBook">Delete Book</a></li>
	  <li><a href="http://localhost:8080/spring/addEmployee">Add Employee</a></li>
	  <li><a href="http://localhost:8080/spring/deleteEmployee">Delete Employee</a></li>
	  <li><a href="http://localhost:8080/spring/report">Generate Report</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	
	<form name="employeeForm" action="home">

	
	
	<br><br>
	  <div align="center">
            <h1>Employee List</h1>
            <table border="1">

                <th>No</th>
                <th>Username</th>
                <th>Email</th>
                <th>Role</th>
                 
                <c:forEach var="user" items="${userList}" varStatus="status">
                <tr>
                   
                  
  					<td>${status.index + 1}</td>
                    <td>	
							<a href="<c:url value="http://localhost:8080/spring/userProfile">  
			               <c:param name="username" value="${user.username}"/> 
			               
			                </c:url>" >${user.username}</a> 
						
					</td>
                    <td>${user.email}</td>
                    <td>${user.role}</td>
                             
                </tr>
                </c:forEach>             
            </table>
         
        </div>
	</form>
	     
	   
	       
		
	
	
	
	     
	   
	       
		
</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>