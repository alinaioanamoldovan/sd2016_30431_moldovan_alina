<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table 
{
    border-collapse: collapse;
}

input[type="text"], textarea 
{
 background-image: url('http://crunchify.com/bg.png');
}

 .nothing {
    border: 0;
    outline: none;
}


</style>
<head>
<meta charset="ISO-8859-1">
<title>HomeEmployee</title>
</head>
<body>
	<ul>
	  <li><a href="http://localhost:8080/spring/homeEmployee">Home</a></li>
	 	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	<div align="center">
	<form name="bookForm"  method = 'Post'>
	
	<table border="0">
	<tr>
		<td><h3>Search  </h3></td>
		<td><h3> <input class="nothing" type='text' th:field="*{search}" name='search' value='...'/></h3></td>
		
	<!--  <td> <input type="submit" name="submit" value="go" class="button"/></td>  -->	
	</tr>
	</table>
	<font color="red">${message}</font>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>
	
	<br><br>
	 	
           
            <table border="1">

                <th>No</th>
                <th>Title</th>
                <th>Author</th>
                <th>Price</th>
                <th>ShoppingCart</th>
                 
                <c:forEach var="books" items="${booksList}" varStatus="status">
                <tr>
                   
                  
  					<td>${status.index + 1}</td>
                    <td>	
							<a href="<c:url value="http://localhost:8080/spring/bookDetailsEmp">  
			               <c:param name="code" value="${books.code}"/> 
			               
			                </c:url>" >${books.title}</a> 
						
					</td>
                    <td>${books.author}</td>
                    <td>${books.price}</td>

                             
                </tr>
                </c:forEach>             
            </table>
         
        </div>
        
	
	     
	

</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>