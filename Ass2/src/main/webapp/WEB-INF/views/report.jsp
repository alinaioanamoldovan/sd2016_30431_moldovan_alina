
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="true" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}


</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report</title>
</head>
<body>
	<ul>
	  <li><a href="http://localhost:8080/spring/homeAdmin">Home</a></li>
	  <li><a href="http://localhost:8080/spring/addBook">Add Book</a></li>
	  <li><a href="http://localhost:8080/spring/viewBook">View Books</a></li>
	  <li><a href="http://localhost:8080/spring/deleteBook">Delete Book</a></li>
	  <li><a href="http://localhost:8080/spring/addEmployee">Add Employee</a></li>
	  <li><a href="http://localhost:8080/spring/deleteEmployee">Delete Employee</a></li>
	  <li><a href="http://localhost:8080/spring/report">Generate Report</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	 <div align="center" >
	 <br><br>

      <form:form action="downloadCSV" method="post" id="downloadCSV">
      <fieldset style="width: 400px;">
          <h3>Spring MVC CSV </h3>
          <input id="submitId" type="submit" value="Download CSV">
      </fieldset>


      </form:form>
         <form:form action="downloadPDF" method="post" id="downloadPDF">
             <h3>Spring MVC PDF Download Example</h3>
             <input id="submitId" type="submit" value="Download PDF">
         </form:form>

     <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
     <font color="red">${message}</font>
     <font color="blue">${message2}</font>
</form> 
</div>
</body>
</html>