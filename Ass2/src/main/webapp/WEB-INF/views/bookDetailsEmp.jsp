
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}

input[type="text"], textarea {

 background-image: url('http://crunchify.com/bg.png');

}

 .nothing {
    border: 0;
    outline: none;
}



</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Details</title>
</head>
<body>
	<ul>
	  <li><a href="http://localhost:8080/spring/homeEmployee">Home</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	
	
	<h2>${title} </h2>
	<h3> General information</h3>
	<form name="editForm"  method = 'Post'>
	<table >
	<tr>
	   <td>Author:</td>  
	   <td>${author}</td>
	</tr>
	
	<tr>
	   <td>Genre:</td>  
	   <td>${genre} </td>
	</tr>
	 
	<tr>
	   <td>Code:</td>  
	   <td>${code} </td>
	</tr>
	 
	</table>
	
	
	<h3> More details</h3>
	<table>
	<tr>
	     <td>Quantity:</td>
	     <td>  
	    
	      <input class="nothing" type='text' th:field="*{quantity}" name='quantity' value='${quantity}'/></td>
	</tr>
	<tr>
		<td>Price:</td>
		<td>   <input class="nothing" type='text' th:field="*{price}" name='price' value='${price}'/></td>
	<tr>
	<tr>
		<td>Description:</td> 
		
	     <td> 
	      <textarea class="nothing" th:field="*{description}" name='description'  rows="3" cols="60">${description}</textarea>
	    <!--   <input class="nothing" type='textarea' th:field="*{description}" name='description' value='${description}'/> -->
	     </td>
     <tr>
	
	</table>
	<input type="submit" name="submit" value="submit" class="button"/>		
	
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    ${message2}		
</form>	
	

</body>
</html>
<%@ include file="/WEB-INF/views/template/footer.jsp" %>