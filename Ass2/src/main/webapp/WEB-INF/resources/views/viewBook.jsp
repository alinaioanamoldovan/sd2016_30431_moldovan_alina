<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>



<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>All Accounts</h1>

            <p class="lead">Checkout all available accounts</p>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr class="bg-success">
                <th>Proto Thumb</th>
                <th>Account owner</th>
                <th>Amount</th>

                <th></th>
            </tr>
            </thead>
            <c:forEach items="${bookList}" var="book" varStatus="status">
                <tr>
                    <td>${book.id}</td>
                    <td>${account.title}</td>

                </tr>
            </c:forEach>
        </table>

<%@ include file="/WEB-INF/views/template/footer.jsp" %>