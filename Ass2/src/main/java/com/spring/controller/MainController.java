package com.spring.controller;


import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spring.model.Book;
import com.spring.model.Books;
import com.spring.model.User;
import com.spring.services.BookService;
import com.spring.services.UserService;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@Controller
public class MainController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BookService bookService;
	
	private ArrayList<Book> cart ;
	private double total;
	
	
	@RequestMapping(value = "/homeAdmin", method = RequestMethod.GET)
	   public String homeAdminPage(Model model) throws IOException 
	   {
		
		   List<User> listUsers= userService.getUsers().getUsers();
		   model.addAttribute("userList", listUsers);
	       return "homeAdmin";
	   }
	@RequestMapping(value = "/homeUser", method = RequestMethod.GET)
	   public String homeUserPage(Model model) throws IOException 
	   {
		   List<Book> listBooks= bookService.getBooks().getBooks();
		   model.addAttribute("booksList", listBooks);
	       return "homeUser";
	   }
	
	@RequestMapping(value = "/homeUser", method = RequestMethod.POST)
	   public String homeUserPagePOST(@RequestParam("search") String search,Model model) throws IOException 
	   {
		 List<Book> listBooks;
		if(search.equals("...") || search.equals(""))
		{
		   listBooks= bookService.getBooks().getBooks();
		   model.addAttribute("booksList", listBooks);
		}
		else
		{
			 listBooks= bookService.getBooksByTitle(search);
			 if (listBooks.size() != 0 )
			 {
				 model.addAttribute("booksList", listBooks);
			 }
			 else 
			 {
				 listBooks= bookService.getBooksByAuthor(search);
				 if (listBooks.size() != 0 )
				 {
					 model.addAttribute("booksList", listBooks);
				 }
				 else
				 {
					 listBooks= bookService.getBooksByGenre(search);
					 if (listBooks.size() != 0 )
					 {
						 model.addAttribute("booksList", listBooks);
					 }
					 else 
					 {
						 model.addAttribute("message", "no results");
						
					 }
				 }
			 }
				 
		}
	       return "homeUser";
	   }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) throws IOException 
	   {
			User e = new User();
		   userService.addUser(e);
	       model.addAttribute("message", "Enter your username/password:");
	       return "login";
	   }
	
	@RequestMapping(value = "/addBook", method = RequestMethod.GET)
	   public String addBookPage(Model model) 
	   {
	       return "addBook";
	   }
	
	@RequestMapping(value = "/addBook", method = RequestMethod.POST)
	   public String addBookPagePOST(Model model, 
			   @RequestParam("title")       String title,
			   @RequestParam("author")      String author,
			   @RequestParam("genre")       String genre,
			   @RequestParam("description") String description,
			   @RequestParam("quantity")    String quantityS,
			   @RequestParam("price")       String priceS,
			   @RequestParam("code")        String code) throws IOException
	   {
		
		  
		   int quantity; 
		   double price;
		
		   
		   
		   // check if all fields are filled 
			if(!title.equals("") && !author.equals("") && !genre.equals("") && 
					!description.equals("") &&  !priceS.equals("") && !code.equals(""))
			{	
				
				if (code.length() != 6 || !quantityS.matches("[0-9]+"))
				   {
					   model.addAttribute("message2","*Invalid code number ");
				   }
				   else 
				   {
				// username must be unique for every User
				   if(bookService.getBookByCode(code)!=null)
				   {
					   model.addAttribute("message2", "*Book already in the store");
				   }
				   else
				   {
					   // code must have 6 characters
					   
						   if(quantityS.equals(""))
						    {
							   quantity = 0;
						    }
						    else
						    {
						    	if(quantityS.matches("[0-9]+")  )
						    	{
						    		
						    		quantity =Integer.parseInt(quantityS);	
						    		
						    	}
						    	else
						    	{
						    		model.addAttribute("message2","*Invalid number");
						    		return "addBook";
						    	}
						   	}
						   
						   if(quantityS.matches("[0-9]+")  )
					    	{
					    		
							   price =Double.parseDouble(priceS);	
							   bookService.addBook(new Book(code,title,author,genre,description,quantity,price)); 
					    	}
					    	else
					    	{
					    		model.addAttribute("message2","*Invalid number");
					    		return "addBook";
					    	}
					   }
					}
			   }
			else
			{
				model.addAttribute("message2", "*All fields must be filled ");
			}
		   return "addBook";
	   }
	
	
	@RequestMapping(value = "/viewBook", method = RequestMethod.GET)
	   public String viewBookPage(Model model) throws IOException 
	   {
		   List<Book> listBooks= bookService.getBooks().getBooks();
		   model.addAttribute("booksList", listBooks);
	       return "viewBook";
	   }
	
	@RequestMapping(value = "/viewBook", method = RequestMethod.POST)
	   public String viewBookPagePOST(Model model) 
	   {
		   return "viewBook";
	   }

	@RequestMapping(value = "/bookDetails", method = RequestMethod.GET)
	   public String bookDetailsPage(@RequestParam String code, Model model) throws IOException 
	   {
		  
		 Book book = bookService.getBookByCode(code);
		 model.addAttribute("code",code);
		 model.addAttribute("title",book.getTitle());
		 model.addAttribute("author",book.getAuthor());
		 model.addAttribute("genre",book.getGenre());
		 model.addAttribute("description",book.getDescription());
		 model.addAttribute("quantity",book.getQuantity());
		 model.addAttribute("price",book.getPrice());
		
	     return "bookDetails";
	   }
	
	@RequestMapping(value = "/bookDetails", method = RequestMethod.POST)
	   public String bookDetailsPagePOST(Model model,
			   @RequestParam("code") String code,
			   @RequestParam("description") String description,
			   @RequestParam("price") String priceS,
			   @RequestParam("quantity") String quantityS) throws IOException 
	   {
		
		Book b = bookService.getBookByCode(code);
		b.setDescription(description);
		int quantity;
		double price;
		
    		
    		quantity =Integer.parseInt(quantityS);	
    		price = Double.parseDouble(priceS);
    	    b.setQuantity(quantity);
    		b.setPrice(price);
    		bookService.updateBook(b);
    		
    	
		
		
		
		 model.addAttribute("code",code);
		 model.addAttribute("title",b.getTitle());
		 model.addAttribute("author",b.getAuthor());
		 model.addAttribute("genre",b.getGenre());
		 model.addAttribute("description",b.getDescription());
		 model.addAttribute("quantity",b.getQuantity());
		 model.addAttribute("price",b.getPrice());
		 
			
	       return "bookDetails";
	   }
	
	
	@RequestMapping(value = "/bookDetailsEmp", method = RequestMethod.GET)
	   public String bookDetailsEmpPage(@RequestParam String code, Model model) throws IOException 
	   {
		  
		 Book book = bookService.getBookByCode(code);
		 model.addAttribute("code",code);
		 model.addAttribute("title",book.getTitle());
		 model.addAttribute("author",book.getAuthor());
		 model.addAttribute("genre",book.getGenre());
		 model.addAttribute("description",book.getDescription());
		 model.addAttribute("quantity",book.getQuantity());
		 model.addAttribute("price",book.getPrice());
		
	     return "bookDetailsEmp";
	   }
	
	@RequestMapping(value = "/bookDetailsEmp", method = RequestMethod.POST)
	   public String bookDetailsEmpPagePOST(Model model,
			   @RequestParam("code") String code,
			   @RequestParam("description") String description,
			   @RequestParam("price") String priceS,
			   @RequestParam("quantity") String quantityS) throws IOException 
	   {
		
		Book b = bookService.getBookByCode(code);
		b.setDescription(description);
		int quantity;
		double price;
		
 		
 		quantity =Integer.parseInt(quantityS);	
 		price = Double.parseDouble(priceS);
 	    b.setQuantity(quantity);
 		b.setPrice(price);
 		bookService.updateBook(b);
 		
 	
		
		
		
		 model.addAttribute("code",code);
		 model.addAttribute("title",b.getTitle());
		 model.addAttribute("author",b.getAuthor());
		 model.addAttribute("genre",b.getGenre());
		 model.addAttribute("description",b.getDescription());
		 model.addAttribute("quantity",b.getQuantity());
		 model.addAttribute("price",b.getPrice());
		 
			
	       return "bookDetailsEmp";
	   }
	
	
	
	@RequestMapping(value = "/deleteBook", method = RequestMethod.GET)
	   public String deleteBookPage(Model model) 
	   {
	       return "deleteBook";
	   }
	
	@RequestMapping(value = "/deleteBook", method = RequestMethod.POST)
	   public String deleteBookPagePOST(@RequestParam("code") String code,Model model) throws IOException 
	   {
		
		
			if(bookService.getBookByCode(code) == null)
			   {
				   model.addAttribute("message", "*Book with code "+code+" does not exist");
			   }
			  else
			  {
				 String msg =  bookService.deleteBook(code);
				 if (msg.equals("book successfully deleted"))
					 model.addAttribute("message2", msg);
				 else 
					 model.addAttribute("message", msg);
			  }
	       return "deleteBook";
	   }   
	

	
	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	   public String addUserPage(Model model) 
	   {
	       return "addUser";
	       
	   }
	
	@RequestMapping(value = "/userProfile", method = RequestMethod.GET)
	   public String userPage(@RequestParam String username,Model model) throws IOException 
	   {
		 User user = userService.getByUsername(username);
		 model.addAttribute("username",username);
		 model.addAttribute("email",user.getEmail());
		 model.addAttribute("firstName",user.getFirstName());
		 model.addAttribute("lastName",user.getLastName());
		 model.addAttribute("role",user.getRole());
		 
		 
	     return "userProfile";
	   }
	
	@RequestMapping(value = "/userProfile", method = RequestMethod.POST)
	   public String userPagePost(@RequestParam("username") String username,
			   @RequestParam("firstName") String firstName,
			   @RequestParam("lastName") String lastName,
			   @RequestParam("email") String email,
			   Model model) throws IOException 
	   {
		
		User e = userService.getByUsername(username);
		e.setFirstName(firstName);
		e.setLastName(lastName);
		e.setEmail(email);
		userService.updateUser(e);
		
		
		
		model.addAttribute("username",username);
		model.addAttribute("email",e.getEmail());
		model.addAttribute("firstName",e.getFirstName());
		model.addAttribute("lastName",e.getLastName());
		model.addAttribute("role",e.getRole());
		 
	     return "userProfile";
	   }
	
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	   public String deleteUserPage(Model model) 
	   {
	       return "deleteUser";
	   }
	
	
	 @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	   public String deleteUser(@RequestParam("username") String username, Model model) throws IOException 
	   {
		  if(userService.getByUsername(username) == null)
		   {
			   model.addAttribute("message", "*User with username "+username+" does not exist");
		   }
		  else
		  {
			 String msg =  userService.deleteUser(username);
			// model.addAttribute("message", msg);
		  }
		       
	           return "deleteUser";
	   }
	 
	 
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	   public String reportPage(Model model) 
	   {
	       return "report";
	   }
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	   
	   public ModelAndView  submit(@RequestParam("username") String username,@RequestParam("password") String password, Model model) throws IOException 
	   {
		  
		
		   
		    if(!username.equals("") && !password.equals(""))
		    { 
		    	User e = userService.getByUsername(username);
		    	if(e!= null)
		    	{
		    		if(e.getPassword().equals(password))
		    			if (e.getRole().equals("ADMIN") )
		    					{
		    						List<User> listUsers= userService.getUsers().getUsers();
		    						model.addAttribute("userList", listUsers);
		    						return  new ModelAndView("redirect:/homeAdmin");
		    					}
		    			else 
		    				{
		    				    List<Book> listBooks= bookService.getBooks().getBooks();
		    				    model.addAttribute("booksList", listBooks);
		    				    cart = new ArrayList<Book>();
		    				    total = 0;
		    				   return  new ModelAndView("redirect:/homeUser");
		    				}
		    		else 
		    		{
		    			model.addAttribute("message2", "*Invalid password");
		    		}
		    	}
		    	else
		    	{
		    		model.addAttribute("message2", "*Account does not exist");
		    	}
		    }
		    
		    else 
		    {
		    	model.addAttribute("message2", "*Empty field username/password");
		    	
		    }
		   
		    return new ModelAndView("redirect:/login");
	   }
	   
	   
	   @RequestMapping(value = "/register", method = RequestMethod.GET)
	   public String registerPage(Model model) 
	   {
	      
		   
	       return "register";
	   }
	   
	   @RequestMapping(value = "/addUser", method = RequestMethod.POST)
	   public String submitRegister( @ModelAttribute User User, Model model) throws IOException 
	   {
	       
		   String username = User.getUsername();
		   String email = User.getEmail();
		   String pass1 = User.getPassword();
		   String fname = User.getFirstName();
		   String lname = User.getLastName();
		   String role = User.getRole();
		 
		 
		 // check if all fields are filled 
		if(!username.equals("") && !pass1.equals("") && !fname.equals("") && 
				!email.equals("") && !lname.equals("") && !role.equals(""))
		{	
				// username must be unique for every User
			   if(userService.getByUsername(username)!=null)
			   {
				   model.addAttribute("message2", "*Username already taken");
			   }
			   else
			   {
				   // password must have more then 6 characters
				   if (pass1.length() < 6)
				   {
					   model.addAttribute("message2","*Password must have at least 6 characters");
				   }
				   else 
				   {
					   // email must contain @ 
					   if(!email.matches("(.*)@(.*).(.*)"))
					   {
						   model.addAttribute("message2","*Invalid email address");
					   }
					   else 
					   {
						 
						   userService.addUser(User);
					   }
				   }
				  
			   }
		   }
		
		
		else
		{
			model.addAttribute("message2", "*All fields must be filled ");
		}
		   return "addUser";
	   }
	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletResponse response) throws IOException {

		response.setContentType("text/csv");
		String reportName = "CSV_Report_Name.csv";
		response.setHeader("Content-disposition", "attachment;filename="+reportName);

		ArrayList<String> rows = new ArrayList<String>();
		rows.add("Title,Author");
		rows.add("\n");
		Books books = bookService.getBooks();
		ArrayList<Book> books1 = books.getBooks();
		for (int i = 0; i < books1.size(); i++) {
			if (books1.get(i).getQuantity()==0) {
				rows.add(books1.get(i).getTitle());
				rows.add(books1.get(i).getAuthor());
				rows.add("\n");
			}
		}

		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}

		response.getOutputStream().flush();

	}
	//private static Font TIME_ROMAN = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	//private static Font TIME_ROMAN_SMALL = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	public Document createPDF(String file) {

		Document document = new Document() {
		};

		try {
			document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();

			addMetaData(document);

			addTitlePage(document);

			this.createTable(document);

			document.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (com.itextpdf.text.DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return document;

	}

	private static void addMetaData(Document document) {
		document.addTitle("Generate PDF report");
		document.addSubject("Generate PDF report");
		document.addAuthor("Java Honk");
		document.addCreator("Java Honk");
	}

	private static void addTitlePage(Document document)
			throws DocumentException, com.itextpdf.text.DocumentException {

		Paragraph preface = new Paragraph();
		creteEmptyLine(preface, 1);
		preface.add(new Paragraph("PDF Report"));

		creteEmptyLine(preface, 1);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		preface.add(new Paragraph("Report created on "
				+ simpleDateFormat.format(new Date())));
		document.add(preface);

	}

	private static void creteEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private void createTable(Document document) throws DocumentException, com.itextpdf.text.DocumentException, IOException {
		Paragraph paragraph = new Paragraph();
		creteEmptyLine(paragraph, 2);
		document.add(paragraph);
		PdfPTable table = new PdfPTable(3);

		PdfPCell c1 = new PdfPCell(new Phrase("Title"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Author"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		//c1 = new PdfPCell(new Phrase("Test"));
		//c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		//table.addCell(c1);
		table.setHeaderRows(1);
		Books books = bookService.getBooks();
		ArrayList<Book> books1 = books.getBooks();
		for (int i = 0; i < books1.size(); i++) {
			table.setWidthPercentage(100);
			table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
			if (books1.get(i).getQuantity()==0) {
				table.addCell(books1.get(i).getTitle());
				//table.addCell("Honk");
				table.addCell(books1.get(i).getAuthor());
			}
		}

		document.add(table);
	}
}
