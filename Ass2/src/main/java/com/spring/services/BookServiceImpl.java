package com.spring.services;

import com.spring.model.Book;
import com.spring.model.Books;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class BookServiceImpl implements BookService 
{
	private static String FILE_NAME = "C\\Users\\Toshiba\\SD_30431_Moldovan_Alina\\Ass2\\src\\books.xml";
	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	
	
	
	public BookServiceImpl()
	{

		 ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");  
		 Marshaller marshaller = (Marshaller)context.getBean("jaxbMarshallerBean"); 
	     Unmarshaller unmarshaller = (Unmarshaller)context.getBean("jaxbMarshallerBean");  
	    
		 this.marshaller  = marshaller;
		 this.unmarshaller  = unmarshaller;
	}
	
	
	
	
	public void addBook(Book book) throws IOException
	{
	  if(getBookByCode(book.getCode()) == null)
	 {
	    Books books = getBooks() ;
		books.getBooks().add(book);
		
		marshaller.marshal(books, new StreamResult(new FileWriter(FILE_NAME)));
	
	  }  
	}
	
	public Books getBooks() throws IOException
	{
		
		 FileInputStream fis = null;
	        try {
	            fis = new FileInputStream(FILE_NAME);
	            Books books = (Books) unmarshaller.unmarshal( new StreamSource(fis));
	    		return books;
	        } finally {
	        	fis.close();
	        }	
	}
	
	public Book getBookByCode(String code) 
	{
		Books books;
		try {
			books = getBooks();
		
		for(Book b : books.getBooks())
	    {
			if(b.getCode().equals(code))
			{
				return b;
			}
	    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Books getBooksOutOfStock() 
	{
		Books books;
		Books outBooks =new Books();
		
		try {
			books = getBooks();
		
		for(Book b : books.getBooks())
	    {
			
			if(b.getQuantity() == 0)
			{
				outBooks.getBooks().add(b);
			}
	    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outBooks;
		
	}
	
	public ArrayList<Book> getBooksByGenre(String genre)
	{
		Books books;
		ArrayList<Book> outBooks =new ArrayList<Book>();
		
		try {
			books = getBooks();
		
		for(Book b : books.getBooks())
	    {
			
			if(b.getGenre().toLowerCase().contains(genre.toLowerCase()))
			{
				
				outBooks.add(b);
			}
	    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outBooks;
		
	}
	
	public ArrayList<Book> getBooksByTitle(String title) 
	{
		Books books;
		ArrayList<Book> outBooks =new ArrayList<Book>();
		
		try {
			books = getBooks();
		
		for(Book b : books.getBooks())
	    {
			
			if(b.getTitle().toLowerCase().contains(title.toLowerCase()))
			{
				outBooks.add(b);
			}
	    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outBooks;
		
	}
	
	public ArrayList<Book> getBooksByAuthor(String author) 
	{
		Books books;
		ArrayList<Book> outBooks =new ArrayList<Book>();
		
		try {
			books = getBooks();
		
		for(Book b : books.getBooks())
	    {
			
			if(b.getAuthor().toLowerCase().contains(author.toLowerCase()))
			{
				outBooks.add(b);
			}
	    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return outBooks;
		
	}
	
	public String deleteBook(String code) throws IOException
	{
		Books books = getBooks();
		for(Book b : books.getBooks())
	    {
			if(b.getCode().equals(code))
			{
				books.getBooks().remove(b);
				marshaller.marshal(books, new StreamResult(new FileWriter(FILE_NAME)));
				return "book successfully deleted";
				
			}
	    }
		
		return "error deleting book";
	}
	
	public String updateBook(Book book) throws IOException
	{
		Books books = getBooks();
		for(Book b : books.getBooks())
	    {
			if(b.getCode().equals(book.getCode()))
			{
				books.getBooks().remove(b);
				books.getBooks().add(book);
				marshaller.marshal(books, new StreamResult(new FileWriter(FILE_NAME)));
				return "book successfully updated";
				
			}
	    }
		
		return "error updating book";
	}
}
