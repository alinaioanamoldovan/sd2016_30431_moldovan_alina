package com.spring.services;

import com.spring.model.User;
import com.spring.model.Users;

import java.io.IOException;


public interface UserService
{

	
	
	public String addUser(User user) throws IOException;
	public Users getUsers() throws IOException;
	public User getByUsername(String username) throws IOException;
	public String deleteUser(String username) throws IOException;
	public String updateUser(User user) throws IOException;
}
