package com.spring.services;

import com.spring.model.User;
import com.spring.model.Users;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;





public class UserServiceImpl implements UserService
	
{
	private static String FILE_NAME = "C\\Users\\Toshiba\\SD_30431_Moldovan_Alina\\Ass2\\src\\users.xml";
	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	
	
	public UserServiceImpl()
	{

		 ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");  
		 Marshaller marshaller = (Marshaller)context.getBean("jaxbMarshallerBean"); 
	     Unmarshaller unmarshaller = (Unmarshaller)context.getBean("jaxbMarshallerBean");  
	    
		 this.marshaller  = marshaller;
		 this.unmarshaller  = unmarshaller;
	}
	
	
	
	
	public String addUser(User user) throws IOException
	{
	  if(getByUsername(user.getUsername()) == null)
	 { 
	//	Users emp = new Users();// sterge asta 
	    Users emp = getUsers() ;
		emp.getUsers().add(user);
		marshaller.marshal(emp, new StreamResult(new FileWriter(FILE_NAME)));
		return "User successfully  added ";
	  }
	  
	  else return "User with username:  " + user.getUsername() + "already exists";
	}
	
	public Users getUsers() throws IOException
	{
		
		 FileInputStream fis = null;
	        try {
	            fis = new FileInputStream(FILE_NAME);
	            Users emp = (Users) unmarshaller.unmarshal( new StreamSource(fis));
	    		return emp;
	        } finally {
	        	fis.close();
	        }

		
	}
	
	public User getByUsername(String username) throws IOException
	{
		Users emp = getUsers();
		for(User e : emp.getUsers())
	    {
			if(e.getUsername().equals(username))
			{
				return e;
			}
	    }
		
		return null;
	}
	
	public String deleteUser(String username) throws IOException
	{
		Users emp = getUsers();
		for(User e : emp.getUsers())
	    {
			if(e.getUsername().equals(username))
			{
				emp.getUsers().remove(e);
				marshaller.marshal(emp, new StreamResult(new FileWriter(FILE_NAME)));
				return "User successfully deleted";
				
			}
	    }
		
		return "error deleting User";
	}
	
	public String updateUser(User user) throws IOException
	{
		Users emp = getUsers();
		for(User e : emp.getUsers())
	    {
			if(e.getUsername().equals(user.getUsername()))
			{
				emp.getUsers().remove(e);
				emp.getUsers().add(user);
				marshaller.marshal(emp, new StreamResult(new FileWriter(FILE_NAME)));
				return "User successfully updated";
				
			}
	    }
		
		return "error updating User";
	}
	
	

}
