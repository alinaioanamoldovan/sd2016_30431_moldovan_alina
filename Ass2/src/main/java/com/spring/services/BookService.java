package com.spring.services;


import com.spring.model.Book;
import com.spring.model.Books;

import java.io.IOException;
import java.util.ArrayList;


public interface BookService {
	
	

	public void addBook(Book book) throws IOException;
	public Books getBooks() throws IOException;
	public Book getBookByCode(String code);
	public Books getBooksOutOfStock();
	public ArrayList<Book> getBooksByGenre(String genre);
	public ArrayList<Book> getBooksByTitle(String title);
	public ArrayList<Book> getBooksByAuthor(String author);
	public String deleteBook(String code) throws IOException;
	public String updateBook(Book book) throws IOException;

}
