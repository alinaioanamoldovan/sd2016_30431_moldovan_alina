package com.spring.model;

import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;




  
@XmlRootElement(name="user")  
public class User 
{  
	private int id;  
	private String firstName; 
	private String lastName;
	private String username;
	private String email;
	private String role;
	private String password;
	
	public User(){}
	
	public User(String fname,String lname, String username,String role, String password,String email, int id)
    {
    	this.firstName = fname;
    	this.lastName  = lname;
    	this.username  = username;
    	this.role      = role;
    	this.password  = password;
    	this.email     = email;
    	this.id        = id;
	}
  
	@XmlAttribute(name="id")  
	public int getId() 
	{  
	    return id;  
	}  
	public void setId(int id) 
	{  
	    this.id = id;  
	}  
	
	
	@XmlElement(name="firstName")  
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@XmlElement(name="lastName")  
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@XmlElement(name="username")  
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@XmlElement(name="email")  
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@XmlElement(name="password")  
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@XmlElement(name="role")  
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}  
	
	 
	  
}  
