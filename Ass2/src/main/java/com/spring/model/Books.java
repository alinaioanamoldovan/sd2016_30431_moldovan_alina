package com.spring.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "books")
@XmlAccessorType (XmlAccessType.FIELD)
public class Books 
{
	 @XmlElement(name = "book")
	    private ArrayList<Book> books ;
	 
	    
	    public Books()
	    {
	    	books = new ArrayList<Book>();
	    }
	    
	    public ArrayList<Book> getBooks() 
	    {
	        return books;
	    }
	 
	    public void setBooks(ArrayList<Book> books) 
	    {
	        this.books = books;
	    }
	    
	    public String toString()
	    {
	    	String s = "";
	    	for(Book b : getBooks())
		    {
				
	    		s = s + b.getTitle() + " " + b.getAuthor() + " " + 
	    				b.getCode()  + " " + b.getPrice()  + "\n";
		    }
	    	
	    	return s;
	    }
}
