package com.spring.model;

import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  
  
@XmlRootElement(name="book")  
public class Book 
{
	
	private String code;
	private String title;
	private String author;
	private String genre;
	private String description;
	private int    quantity;
	private double  price;
	private boolean outOfStock;
	
	public Book(){}
	
	public Book(String code, String titl, String author, String genre, String des, int q, double p)
	{
		this.code        = code;
		this.title       = titl;
		this.author      = author;
		this.genre       = genre;
		this.description = des;
		this.quantity    = q;
		this.price       = p;
		if (quantity == 0 ) outOfStock = true;
		else  outOfStock = false;
	}
	
	@XmlAttribute(name="code")  
	public String getCode() 
	{
		return code;
	}
	public void setCode(String code) 
	{
		this.code = code;
	}
	@XmlElement(name="title")  
	public String getTitle() 
	{
		return title;
	}
	public void setTitle(String title) 
	{
		this.title = title;
	}
	@XmlElement(name="author")  
	public String getAuthor()
	{
		return author;
	}
	public void setAuthor(String author) 
	{
		this.author = author;
	}
	@XmlElement(name="genre")  
	public String getGenre() 
	{
		return genre;
	}
	public void setGenre(String genre)
	{
		this.genre = genre;
	}
	@XmlElement(name="description")  
	public String getDescription() 
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	@XmlElement(name="quantity")  
	public int getQuantity() 
	{
		return quantity;
	}
	public void setQuantity(int quantity) 
	{
		if (quantity >= 0 )
		 this.quantity = quantity;
		
		if (quantity == 0 ) outOfStock = true;
		else  outOfStock = false;
	
	}
	
	public boolean getOutOfStock()
	{
		return this.outOfStock;
	}
	
	@XmlElement(name="price")  
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price) 
	{
		this.price = price;
	}

}
