package com.clinic.controllers;

import com.clinic.model.Programare;
import com.clinic.service.DoctorService;
import com.clinic.service.PatientService;
import com.clinic.service.ProgramareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by Toshiba on 5/25/2016.
 */
@Controller
public class ProgramareController {
    @Autowired
    private PatientService patientService;
    @Autowired
    private ProgramareService programareService;
    @Autowired
    private DoctorService doctorService;

    @RequestMapping(value = "/programari", method = RequestMethod.GET)
    public ModelAndView listPatients() {

        ModelAndView mav = new ModelAndView("/programare/listProgramari");
        mav.addObject("listProgramari", this.programareService.listProgramares());

        return mav;
    }

    @RequestMapping(value = "/programare/add", method = RequestMethod.GET)
    public ModelAndView addPatient(Model model) {

        ModelAndView mav = new ModelAndView();
        mav.setViewName("/programare/addProgramare");

        model.addAttribute("programare", new Programare());
        model.addAttribute("patient", this.patientService.listPatients());
        model.addAttribute("doctors", this.doctorService.listDoctors());

        return mav;
    }

    @RequestMapping(value= "/programare/add", method = RequestMethod.POST)
    public String addPatient(@Valid @ModelAttribute("programare") Programare programare,
                             BindingResult bindingResult, final RedirectAttributes redirectAttributes){

        if(bindingResult.hasErrors()) {
            return "programare/addProgramare";
        }

        if(programare.getId() == 0){

                this.programareService.addProgramare(programare);
        }

           // redirectAttributes.addFlashAttribute("message", "The patient has been successfully created");
        else{
            this.programareService.updateProgramare(programare);
            //redirectAttributes.addFlashAttribute("message", "The patient has been successfully updated");
        }

        return "redirect:/programari";
    }

    @RequestMapping(value = "/programare/{id}/edit", method = RequestMethod.GET)
    public ModelAndView editPatient(Model model, @PathVariable("id") int id) {
        ModelAndView mav = new ModelAndView("programare/addProgramare");


        Programare programare = this.programareService.getProgramareById(id);
        model.addAttribute("programare", programare);
        model.addAttribute("patients", this.patientService.listPatients());
        model.addAttribute("doctors", this.doctorService.listDoctors());

        return mav;
    }

    @RequestMapping(value = "/programare/{id}/remove")
    public String removePatient(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        this.programareService.removeProgramare(id);
       // redirectAttributes.addFlashAttribute("message", "The patient has been successfully deleted");

        return "redirect:/patient/listProgramari";
    }
}
