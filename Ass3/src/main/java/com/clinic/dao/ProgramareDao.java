package com.clinic.dao;

import com.clinic.model.Programare;

import java.util.List;

/**
 * Created by Toshiba on 5/25/2016.
 */
public interface ProgramareDao {
    public void addProgramare(Programare Programare);
    public void updateProgramare(Programare Programare);
    public List<Programare> listProgramares();
    public Programare getProgramareById(int id);
    public void removeProgramare(int id);
}
