package com.clinic.dao;

import com.clinic.model.Programare;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Toshiba on 5/25/2016.
 */
@Repository
public class ProgramareDaoImpl implements ProgramareDao{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addProgramare(Programare programare) {
        this.entityManager.persist(programare);
    }

    @Override
    public void updateProgramare(Programare programare) {
        this.entityManager.merge(programare);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Programare> listProgramares() {
        List<Programare> programarelist = this.entityManager.createQuery("SELECT pat FROM Programare pat ORDER BY pat.date").getResultList();

        return programarelist;
    }

    @Override
    public Programare getProgramareById(int id) {
        return this.entityManager.find(Programare.class, id);
    }

    @Override
    public void removeProgramare(int id) {
        Programare programare = this.getProgramareById(id);
        if (null != programare) {
            this.entityManager.remove(programare);
        }
    }
}
