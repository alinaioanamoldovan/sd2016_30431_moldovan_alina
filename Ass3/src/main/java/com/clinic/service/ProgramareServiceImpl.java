package com.clinic.service;

import com.clinic.dao.ProgramareDao;
import com.clinic.model.Programare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Toshiba on 5/25/2016.
 */
@Service
public class ProgramareServiceImpl implements ProgramareService {
    @Autowired
    private ProgramareDao programareDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addProgramare(Programare programare)   {
        this.programareDAO.addProgramare(programare);
    }

    @Transactional
    @Override
    public void updateProgramare(Programare programare) {
        this.programareDAO.updateProgramare(programare);
    }

    @Transactional
    @Override
    public List<Programare> listProgramares() {
        return this.programareDAO.listProgramares();
    }

    @Transactional
    @Override
    public Programare getProgramareById(int id) {
        return this.programareDAO.getProgramareById(id);
    }

    @Transactional
    @Override
    public void removeProgramare(int id) {
        this.programareDAO.removeProgramare(id);
    }
}
