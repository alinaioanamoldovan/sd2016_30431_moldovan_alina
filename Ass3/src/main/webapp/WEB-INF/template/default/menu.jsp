<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-brand" href="<c:url value='/' />">Home</a>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<c:url value="/logout"/> "><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav in" id="side-menu">
                <li>
                    <a href="<c:url value='/' />" ><spring:message code="indexPage"/></a>
                </li>
                <li>
                    <a href="<c:url value='/doctors' />" ><spring:message code="doctors"/></a>
                </li>
                <li>
                    <a href="<c:url value='/patients' />" ><spring:message code="patients"/></a>
                </li>
                <li>
                <a href="<c:url value='/chat' />" ><spring:message code="chat"/></a>
            </li>
                <li>
                    <a href="<c:url value='/addProgramare' />" >Programare</a>
                </li>
                </form>
                <spring:message code="add"/> <a href="<c:url value="/addUser" />"><spring:message code="register"/></a>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
</nav>
