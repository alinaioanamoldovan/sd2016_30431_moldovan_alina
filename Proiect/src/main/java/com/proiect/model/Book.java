package com.proiect.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

@Entity
public class Book implements Serializable {

    private static final long serialVersionUID = -3532377236419382983L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int bookId;

    @NotEmpty(message = "The Book name must not be empty")
    private String bookName;

    private String bookCategory;
    private String bookDescription;

    @Min(value = 0, message = "The Book price must not be less then zero")
    private double bookPrice;



    @Min(value = 0, message = "The Book unit must not be less then zero")
    private int unitInStock;
    private String bookManufacturer;

    @Transient
    private MultipartFile bookImage;

    @OneToMany(mappedBy = "Book", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<CartItem> cartItemList;


    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookCategory() {
        return bookCategory;
    }

    public void setBookCategory(String bookCategory) {
        this.bookCategory = bookCategory;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String BookDescription) {
        this.bookDescription = BookDescription;
    }

    public double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(double bookPrice) {
        this.bookPrice = bookPrice;
    }


    public int getUnitInStock() {
        return unitInStock;
    }

    public void setUnitInStock(int unitInStock) {
        this.unitInStock = unitInStock;
    }

    public String getBookManufacturer() {
        return bookManufacturer;
    }

    public void setBookManufacturer(String BookManufacturer) {
        this.bookManufacturer = BookManufacturer;
    }


    public MultipartFile getBookImage() {
        return bookImage;
    }

    public void setBookImage(MultipartFile BookImage) {
        this.bookImage = BookImage;
    }


    public List<CartItem> getCartItemList() {
        return cartItemList;
    }

    public void setCartItemList(List<CartItem> cartItemList) {
        this.cartItemList = cartItemList;
    }
} // The End of Class;
