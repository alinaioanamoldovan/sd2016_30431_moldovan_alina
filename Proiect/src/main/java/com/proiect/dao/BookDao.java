package com.proiect.dao;


import com.proiect.model.Book;

import java.util.List;

public interface BookDao {

    List<Book> getBookList();

    Book getBookById (int id);

    void addBook(Book book);

    void editBook(Book book);

    void deleteBook(Book book);
}
