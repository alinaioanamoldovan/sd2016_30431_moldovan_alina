package com.proiect.controller.admin;

import com.proiect.model.Book;
import com.proiect.model.Customer;
import com.proiect.service.BookService;
import com.proiect.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminHome {

    @Autowired
    private BookService bookService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping
    public String adminPage(){
        return "admin";
    }

    @RequestMapping("/bookInventory")
    public String bookInventory(Model model){
        List<Book> books = bookService.getBookList();
        model.addAttribute("books", books);

        return "bookInventory";
    }

    @RequestMapping("/customer")
    public String customerManagerment(Model model){

        List<Customer> customerList = customerService.getAllCustomers();
        model.addAttribute("customerList", customerList);

        return "customerManagement";
    }


}
