package com.proiect.controller;


import com.proiect.model.Book;
import com.proiect.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping("/bookList/all")
    public String getBooks(Model model){
        List<Book> books = bookService.getBookList();
        model.addAttribute("books", books);

        return "bookList";
    }

    @RequestMapping("/viewBook/{bookId}")
    public String viewBook(@PathVariable int bookId, Model model) throws IOException{
        Book book = bookService.getBookById(bookId);
        model.addAttribute("book", book);

        return "viewBook";
    }

    @RequestMapping("/bookList")
    public String getBookByCategory(@RequestParam("searchCondition") String searchCondition, Model model){
        List<Book> books = bookService.getBookList();
        model.addAttribute("books", books);
        model.addAttribute("searchCondition", searchCondition);

        return "bookList";
    }

} // The End of Class;
