package com.proiect.service.impl;


import com.proiect.dao.BookDao;
import com.proiect.model.Book;
import com.proiect.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao bookDao;

    public Book getBookById(int bookId){
        return bookDao.getBookById(bookId);
    }

    public List<Book> getBookList(){
        return bookDao.getBookList();
    }

    public void addBook(Book book){
        bookDao.addBook(book);
    }

    public void editBook(Book book){
        bookDao.editBook(book);
    }

    public void deleteBook(Book book){
        bookDao.deleteBook(book);
    }


} // The End of Class;
