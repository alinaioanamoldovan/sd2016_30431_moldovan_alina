package com.proiect.service;


import com.proiect.model.Book;

import java.util.List;

public interface BookService {

    List<Book> getBookList();

    Book getBookById (int id);

    void addBook(Book book);

    void editBook(Book book);

    void deleteBook(Book book);

}
