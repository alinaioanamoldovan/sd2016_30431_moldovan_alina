package com.proiect.service;

import com.proiect.model.Cart;

public interface CartService {

    Cart getCartById(int cartId);

    void update(Cart cart);
}
