<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Add Book</h1>


            <p class="lead">Fill the below information to add a book:</p>
        </div>

        <form:form action="${pageContext.request.contextPath}/admin/book/addBook"
                   method="post" commandName="book" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Name</label>
                <form:errors path="bookName" cssStyle="color:#ff0000;" />
                <form:input path="bookName" id="name" class="form-Control" />
            </div>

        <div class="form-group">
            <label for="name">Genre</label>
            <form:errors path="bookCategory" cssStyle="color:#ff0000;" />
            <form:input path="bookCategory" id="name" class="form-Control" />
        </div>

            <div class="form-group">
                <label for="description">Description</label>
                <form:textarea path="bookDescription" id="description" class="form-Control" />
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <form:errors path="bookPrice" cssStyle="color:#ff0000;" />
                <form:input path="bookPrice" id="price" class="form-Control" />
            </div>

        <div class="form-group">
                <label for="unitInStock">Unit In Stock</label>
                <form:errors path="unitInStock" cssStyle="color:#ff0000;" />
                <form:input path="unitInStock" id="unitInStock" class="form-Control" />
            </div>

            <div class="form-group">
                <label for="manufacturer">Editura</label>
                <form:input path="bookManufacturer" id="manufacturer" class="form-Control" />
            </div>

            <div class="form-group">
                <label class="control-label" for="bookImage">Upload Picture</label>
                <form:input id="bookImage" path="bookImage" type="file" class="form:input-large" />
            </div>

        <br/><br/>

        <input type="submit" value="submit" class="btn btn-default">
        <a href="<c:url value="/admin/bookInventory" />" class="btn btn-default">Cancel</a>

    </form:form>

<%@ include file="/WEB-INF/views/template/footer.jsp" %>