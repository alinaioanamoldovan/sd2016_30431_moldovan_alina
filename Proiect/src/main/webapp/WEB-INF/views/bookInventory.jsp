<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>


<script>
    $(document).ready(function(){

        $('.table').DataTable({
            "lengthMenu": [[1,2,3,5,10, -1], [1,2,3,5,10, "All"]]
        });
    });
</script>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Book Inventory Page</h1>


            <p class="lead">This is the book inventory page:</p>

        <table class="table table-striped table-hover">
            <thead>
            <tr class="bg-success">
                <th>Proto Thumb</th>
                <th>Book Name</th>
                <th>Genre</th>
                <th>Price</th>
                <th></th>
            </tr>
            </thead>
            <c:forEach items="${books}" var="book">
                <tr>
                    <td><img src="<c:url value="/resources/images/${book.bookId}.png" />" alt="image" style="width:100%"/></td>
                    <td>${book.bookName}</td>
                    <td>${book.bookCategory}</td>
                    <td>${book.bookPrice} USD </td>
                    <td>
                        <a href="<spring:url value="/book/viewBook/${book.bookId}" />"><span class="glyphicon glyphicon-info-sign"></span></a>
                        <a href="<spring:url value="/admin/book/deleteBook/${book.bookId}" />"><span class="glyphicon glyphicon-remove"></span></a>
                        <a href="<spring:url value="/admin/book/editBook/${book.bookId}" />"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <a href="<spring:url value="/admin/book/addBook" />" class="btn btn-primary">Add Book</a>

<%@ include file="/WEB-INF/views/template/footer.jsp" %>