import java.sql.*;
public class Tema2 {
	  // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost:3306/lab2.2";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "";
	   static PreparedStatement pstmt = null;
	   static Connection conn = null;
	   public static String insert(int ID,String name,String birthdate,String address,int age) throws SQLException
	   {
		   String sql2="INSERT INTO `student`(idstudent,Name,Birthdate,Address,Age) "+
                   "VALUES (?,?,?,?,?)";
		   String dateDespre=ID+name+birthdate+address+age;
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1, ID);
				pstmt.setString(2, name); // set input parameter 2
			    pstmt.setString(3,birthdate ); // set input parameter 3
			    pstmt.setString(4, address);
			    pstmt.setInt(5, age);
			    pstmt.executeUpdate();
				// TODO Auto-generated catch block
				   return dateDespre;
	   
	   }
	   public static void insertCourse(int ID,String name,String teacher,int year) throws SQLException
	   {
		   String sql2="INSERT INTO `course`(idcourse,Name,Teacher,Year) "+
                   "VALUES (?,?,?,?)";
		      
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1, ID);
				pstmt.setString(2, name); // set input parameter 2
			    pstmt.setString(3, teacher); // set input parameter 3
			    pstmt.setInt(4, year);
			    pstmt.executeUpdate();
				// TODO Auto-generated catch block
				//e.printStackTrace();
		// set input parameter 1
		       // execute insert statement
		   
	   
	   }
	   public static void enroll(int ID,String name,String course) throws SQLException
	   {
		   String sql2="INSERT INTO `enroll`(idenroll,student,course) "+
                   "VALUES (?,?,?,?)";
		      
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1, ID);
				pstmt.setString(2, name); // set input parameter 2
			    pstmt.setString(3, course); // set input parameter 3
			    pstmt.executeUpdate();
				// TODO Auto-generated catch block
				//e.printStackTrace();
		// set input parameter 1
		       // execute insert statement
		   
	   
	   }
	   public static void update(int age,int ID) throws SQLException
	   {
		   String sql = "UPDATE `student` set age=? where idstudent = ?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setInt(1,age);
		   pstmt.setInt(2, ID);
		   pstmt.executeUpdate();
	   }
	   public static void updateCourse(int year,String name) throws SQLException
	   {
		   String sql = "UPDATE `course` set year=? where teacher=?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setInt(1,year);
		   pstmt.setString(2, name);
		   pstmt.executeUpdate();
		   
	   }
	   public static void delete(int ID) throws SQLException
	   {
		   String sql = "DELETE FROM `student` WHERE idstudent=?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setInt(1,ID);
		   pstmt.executeUpdate();
	   }
	   public static void deleteCourse(int ID) throws SQLException
	   {
		   String sql = "DELETE FROM `course` WHERE idstudent=?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setInt(1,ID);
		   pstmt.executeUpdate();
	   }
	   public static void view(String name) throws SQLException
	   {
		   String sql="SELECT idstudent,Name,Address,Age FROM student WHERE name=?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setString(1,name);
		  ResultSet rs=pstmt.executeQuery();
		  
			  while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("idstudent");
		         int age = rs.getInt("Age");
		         String first = rs.getString("Name");
		         String last = rs.getString("Address");

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Age: " + age);
		         System.out.print(", Name: " + first);
		         System.out.println(", Address: " + last);
		      }
			  rs.close();
		  //System.out.println(rs.getString(1));
		      
		  
	   }
	   public static void viewEnroll(String name) throws SQLException
	   {
		   String sql="SELECT idenroll,student,course FROM enroll WHERE course=? ORDER BY idenroll ASC ";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setString(1,name);
		  ResultSet rs=pstmt.executeQuery();
		  
			  while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("idenroll");
		         String first = rs.getString("student");
		         String last = rs.getString("course");

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Name: " + first);
		         System.out.println(", Course: " + last);
		      }
			  rs.close();
		  //System.out.println(rs.getString(1));
		      
		  
	   }
	   public static void viewCourse(String name) throws SQLException
	   {
		   String sql="SELECT idcourse,Name,Teacher,Year FROM course WHERE name=?";
		   pstmt=conn.prepareStatement(sql);
		   pstmt.setString(1,name);
		  ResultSet rs=pstmt.executeQuery();
		  
			  while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("idcourse");
		         int age = rs.getInt("Year");
		         String first = rs.getString("Name");
		         String last = rs.getString("Teacher");

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Teaching Year: " + age);
		         System.out.print(", Name: " + first);
		         System.out.println(", Teacher: " + last);
		      }
			  rs.close();
		  //System.out.println(rs.getString(1));
		      
		  
	   }
	   public static Connection getConnection() throws Exception {
		    Class.forName("com.mysql.jdbc.Driver");
		    Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
		   
		    return conn;
	   }

	   public static void main(String[] args) throws Exception
	   {
		  
		   Statement stmt = null;
		   conn=getConnection();
			System.out.println("Connected");
			      //STEP 2: Register JDBC driver
			     

			      //STEP 3: Open a connection
			     // System.out.println("Connecting to database...");
			      //conn = DriverManager.getConnection(DB_URL, USER, PASS);
                  //System.out.println("Connected");
			      //STEP 4: Execute a query
			      //System.out.println("Insert into table");
			    /* stmt = conn.createStatement();
			      String sql = "INSERT INTO student " +
		                   "VALUES (1, 'Moldovan Alina', '12/07/1994','Blaj,nr 29',21)";
		          stmt.executeUpdate(sql);
		           /*String sql2="INSERT INTO student "+
		                   "VALUES (2,'Ion Popescu','13/01/1980','Cluj Napoca,strada Primaveri,nr 20')";
		           stmt.executeUpdate(sql2);*/
		          //insert(2,"Adriana Marinescu","15/06/1989","Cluj Napoca",27);
		          //System.out.println("Inserted");
			     // update(21,2);
                  //delete(1);
                 view("Adriana Marinescu");
                  //insertCourse(1,"Introduction to Computer Programming","Ion Popescu",1);
			
		   }
}
