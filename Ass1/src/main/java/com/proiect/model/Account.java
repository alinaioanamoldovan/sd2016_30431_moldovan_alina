package com.proiect.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Moldovan on 4/8/2016.
 */
@Entity
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int accountId;
    @NotEmpty(message="The account number cannot be empty")
   @Size(min=24, max=24,message = "too many numbers")
    private String accountNum;
  @NotEmpty(message="The account owner cannot be empty")
  // @JsonIgnore
    private String owner;
    private String accountType;
   @Min(value = 0, message = "The account balance must not be less then zero")
   private double amount;
   @NotEmpty(message="The account open date cannot be empty")
    private String openDate;

    @Transient
    private MultipartFile accountImage;
    public  int  getAccountId() {
        return accountId;
    }
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
    public String getAccountNum() {
        return accountNum;
    }
    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }
    public String getOwner()
    {
        return owner;
    }
    public void setOwner(String owner)
    {
        this.owner=owner;
    }
    public String getAccountType() {
        return accountType;
    }
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public MultipartFile getAccountImage() {
        return accountImage;
    }

    public void setAccountImage(MultipartFile accountImage) {
        this.accountImage = accountImage;
    }
}
