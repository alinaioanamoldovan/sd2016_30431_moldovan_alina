package com.proiect.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Moldovan on 4/10/2016.
 */
@Entity
public class Client  implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int clientId;
    @NotEmpty(message="The client first  cannot be empty")
    private String fname;
    @NotEmpty(message="The client last cannot be empty")
    private String lname;
    @NotEmpty(message="The date of birth cannot be empty")
    private String dob;
    @NotEmpty(message="The address cannot be empty")
    private String address;
    @NotEmpty(message="The city cannot be empty")
    private String city;
    @NotEmpty(message="The zipcode cannot be empty")
    private String zipcode;
    @NotEmpty(message="The CNP cannot be empty")
    //@Pattern(regexp="^(1|[1-9][0-9]*)$")
    @Size(min=13, max=13,message = "too many numbers")
    private String CNP;

    @Size(min=10, max=10,message = "too many numbers")
    private String phone;
    @NotEmpty(message="The email address cannot be empty")
    private String emailId;
    //@OneToMany(mappedBy = "client",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonIgnore
    private String accountNum;


    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }



    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }
}
