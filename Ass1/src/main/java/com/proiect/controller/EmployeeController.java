package com.proiect.controller;

import com.proiect.dao.*;
import com.proiect.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transaction;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;

/**
 * Created by Moldovan on 4/16/2016.
 */
@Controller
@RequestMapping("/employee")
public class EmployeeController {
    private Path path;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private TransactionDao transactionDao;
    @Autowired
    private BillDao billDao;
    @Autowired
    private UserDao userDao;

    @RequestMapping("/accountList")
    public String getAccount(Model model) {
        List<Account> accounts = accountDao.getAllAccounts();
        //Account account=accountList.get(0);
        model.addAttribute("accounts", accounts);
        return "accountList";
    }


    @RequestMapping(value = "/addAccount",method = RequestMethod.GET)
    public ModelAndView addAccount() {
        ModelAndView model=new ModelAndView("addAccount");
        Account account = new Account();
        account.setAccountType("savings");
        model.addObject("client",clientDao.getAllClients());
        model.addObject("account", account);
        return model;
    }

    @RequestMapping(value = "/addAccount", method = RequestMethod.POST)
    public String addAccountPost(@Valid @ModelAttribute("account") Account account,BindingResult result,Model model,  HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("client", clientDao.getAllClients());
            model.addAttribute("bill", new Account());
            System.out.println(result.toString());
            return "addAccount";
        }
        accountDao.addAccount(account);

        MultipartFile accountImage = account.getAccountImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "/WEB-INF/resources/images/" + account.getAccountId() + ".jpg");

        if (accountImage != null && !accountImage.isEmpty()) {
            try {
                accountImage.transferTo(new File(path.toString()));
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new RuntimeException("Product image saving failed", ex);
            }
        }
        return "redirect:/employee/accountList";
    }

    @RequestMapping("/accountList/viewAccount/{accountId}")
    public String viewAccount(@PathVariable int accountId, Model model) throws IOException {
        Account account = accountDao.getAccountById(accountId);
        model.addAttribute(account);

        return "viewAccounts";


    }

    @RequestMapping("/deleteAccount/{accountId}")
    public String deleteAccount(@PathVariable int accountId, Model model, HttpServletRequest request) {

        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "/WEB-INF/resources/images/" + accountId + ".jpg");
        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException ex) {
                ex.printStackTrace();
                //throw new RuntimeException("Cannot delete file");
            }
        }
        Account account= accountDao.getAccountById(accountId);
        accountDao.deleteAccount(accountId);
        Bill bill = billDao.getBillByAccountNum(account.getAccountNum());
        Transactions tr= transactionDao.findByAccountNum(account.getAccountNum());

        billDao.deleteBill(bill.getBillId());
        transactionDao.deleteTransaction(tr);

        return "redirect:/employee/accountList";


    }

    @RequestMapping("/editAccount/{accountId}")
    public String editAccount(@PathVariable int accountId, Model model) {
        Account account = accountDao.getAccountById(accountId);
        model.addAttribute(account);

        return "editAccount";
    }

    @RequestMapping("/editClient/{clientId}")
    public String editClient(@PathVariable int clientId, Model model) {
        Client client = clientDao.getClientById(clientId);
        model.addAttribute(client);
        return "editClient";
    }

    @RequestMapping(value = "/editAccount", method = RequestMethod.POST)
    public String editAccountPost(@Valid @ModelAttribute("account") Account account, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "editAccount";
        }
        MultipartFile accountImage = account.getAccountImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        path = Paths.get(rootDirectory + "/WEB-INF/resources/images/" + account.getAccountId() + ".jpg");
        if (accountImage != null && !accountImage.isEmpty()) {
            try {
                accountImage.transferTo(new File(path.toString()));
            } catch (Exception e) {
                e.printStackTrace();
                new RuntimeException("Update image failed", e);
            }
            {

            }
        }
        accountDao.editAccount(account);
        return "redirect:/employee/accountList";
    }

    @RequestMapping(value = "/editClient", method = RequestMethod.POST)
    public String editClientPost(@Valid @ModelAttribute("client") Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "editClient";
        }

        clientDao.editClient(client);
        //model.addAttribute(client);
        return "redirect:/employee/clientList";
    }

    @RequestMapping("/deleteClient/{clientId}")
    public String deleteClient(@PathVariable int clientId, Model model) {
        Client client = clientDao.getClientById(clientId);
        //String name = client.getFname()+client.getLname();
        //Account account= accountDao.getAccountByOwner(name);
       // accountDao.deleteAccount(account.getAccountId());
        clientDao.deleteClient(clientId);

        return "redirect:/employee/clientList";
    }

    @RequestMapping("/clientList")
    public String getClient(Model model) {
        List<Client> clients = clientDao.getAllClients();
        model.addAttribute("clients", clients);
        return "clientList";
    }

    @RequestMapping("/addClient")
    public String addClient(Model model) {
        Client client = new Client();
        //account.setAccountType("savings");
        //Account account=new Account();
        //client.setAccounts();
        List<Client> clientList = clientDao.getAllClients();
        model.addAttribute("client", client);
        return "addClient";
    }


    @RequestMapping(value = "/addClient", method = RequestMethod.POST)
    public String addClientPost(@Valid @ModelAttribute("client") Client client, BindingResult result, HttpServletRequest request,Model model) {
        //Set<Account> accounts = new HashSet<Account>();
        // client.setAccounts(accounts);
        if (result.hasErrors()) {
            return "addClient";
        }

        for (int i = 0; i < clientDao.getAllClients().size(); i++) {
            if (client.getCNP().equals(clientDao.getAllClients().get(i).getCNP())) {
                model.addAttribute("cnpMsg", "CNP alredy exists");

                return "addClient";
            }
        }
        clientDao.addClient(client);


        return "redirect:/employee/clientList";
    }

    @RequestMapping("/clientList/viewClient/{clientId}")
    public String viewClient(@PathVariable int clientId, Model model) throws IOException {
        Client client = clientDao.getClientById(clientId);
        model.addAttribute(client);

        return "viewClients";


    }


   /* @Autowired
    TransactionDao transactionDao;
    @RequestMapping(value = "/makeTransfer", method = RequestMethod.GET)
    public ModelAndView saveBill() {

        ModelAndView modelAndView = new ModelAndView("makeTransfer");

        modelAndView.addObject("accounts", accountDao.getAllAccounts());
        modelAndView.addObject("transactions", new Transactions());

        return modelAndView;
    }*/
   private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);
   public Users getLoggedInfo()
   {
       Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

       if (principal instanceof Users) {
           Users username = ((Users) principal);
           return username;
       }
      //return null;
       return null;
   }
    public static Authentication getAuthentication(boolean strict) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (strict && authentication == null) {
            throw new AuthenticationCredentialsNotFoundException("Missing authentication object.");
        }
        return authentication;
    }

    /**
     * Get active person principal.
     * @return Active person principal user. Never null.
     * @throws AccessDeniedException in case there is no active person principal.
     */
    public static Users getActivePersonPrincipal() throws AccessDeniedException {
        Object principal = getAuthentication(true).getPrincipal();
        if (!(principal instanceof Users)) {
            throw new AccessDeniedException("Invalid principal '" + principal + "'.");
        }
        return (Users) principal;
    }

    @RequestMapping(value = "/makeBill", method = RequestMethod.GET)
   public ModelAndView saveBill() {

       ModelAndView modelAndView = new ModelAndView("makeBill");

       modelAndView.addObject("accounts", accountDao.getAllAccounts());
       modelAndView.addObject("bill", new Bill());

       return modelAndView;
   }

    @RequestMapping(value = "/makeBill", method = RequestMethod.POST)
    public String addBill(@Valid @ModelAttribute("bill") Bill bill, BindingResult result, ModelMap model,Principal principal) {
        if(result.hasErrors()){
            model.addAttribute("accounts", accountDao.getAllAccounts());
            model.addAttribute("bill", new Bill());
            System.out.println(result.toString());
            return "makeBill";
        }


       // bill.setUser(HomeController.getUser());
        Account requestedBillAcount = accountDao.getAccountByAccountNum(bill.getAccount());
        requestedBillAcount.setAmount(requestedBillAcount.getAmount() - bill.getAmount());
        accountDao.updateAccount(requestedBillAcount,bill.getAmount());

       // bill.setDate(new Date());
        billDao.addBill(bill);
        return "redirect:/employee/accountList";
    }
    @RequestMapping(value = "/makeTransfer", method = RequestMethod.GET)
    public ModelAndView saveTransfer() {

        ModelAndView modelAndView = new ModelAndView("makeTransfer");

        modelAndView.addObject("accounts", accountDao.getAllAccounts());
        modelAndView.addObject("transaction", new Transactions());

        return modelAndView;
    }

    @RequestMapping(value = "/makeTransfer", method = RequestMethod.POST)
    public String addBill(@Valid @ModelAttribute("transaction") Transactions transaction, BindingResult result, ModelMap model) {
        if(result.hasErrors()){
            model.addAttribute("accounts", accountDao.getAllAccounts());
            model.addAttribute("transaction", new Transactions());
            System.out.println(result.toString());
            return "makeTransfer";
        }



        Account fromAccount = accountDao.getAccountByAccountNum(transaction.getFromAccount());
        Account toAccount = accountDao.getAccountByAccountNum(transaction.getToAccount());
        if (fromAccount.getAccountNum().equals(toAccount.getAccountNum()))
        {
            model.addAttribute("accounts", accountDao.getAllAccounts());
            model.addAttribute("transaction", new Transactions());
            System.out.println(result.toString());
            return "redirect:/employee/accountList";

        }
        else {
            if (transaction.getAmount() <= toAccount.getAmount()) {
                fromAccount.setAmount(fromAccount.getAmount() - transaction.getAmount());
                toAccount.setAmount(toAccount.getAmount() + transaction.getAmount());
                accountDao.updateAccount(fromAccount, fromAccount.getAmount() - transaction.getAmount());
                accountDao.updateAccount(toAccount, toAccount.getAmount() + transaction.getAmount());
              //  transaction.setDate(new Date());
                transactionDao.addNewTransaction(transaction);

            } else
            {
                model.addAttribute("amountMsg","Insufficient funds");
            }

        }
                return "redirect:/employee/clientList";
    }
}
