package com.proiect.controller;

import com.proiect.dao.UserDao;
import com.proiect.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Moldovan on 4/8/2016.
 */
@Controller
public class HomeController {
    private static Users loggedUser;
    @RequestMapping("/")
    public String home()
    {
        return "home";
    }
    @Autowired
    private UserDao userDao;

    @RequestMapping("/login")
    public ModelAndView login(@ModelAttribute("user") Users user,
                              @RequestParam(value = "error", required = false)
            String error,
                              @RequestParam(value = "logout", required = false)
            String logout) {
        ModelAndView model=new ModelAndView("login");

        if (error != null) {
            model.addObject("error", "Invalid username and password");
        }

        if (logout != null) {
            model.addObject("msg", "You have been logged out successfully");
        }
        for(Users user1:userDao.getAllUsers())
        {
            if (user1.getUsername().equals(user.getUsername())
                    && user1.getPassword().equals(user.getPassword()))
            {
                setUser(user1);
                model.addObject("user",user1);
            }
            return model;
        }
        return null;
    }
    public static void setUser(Users user) {
         loggedUser = user;
    }

    public static Users getUser() {
        return loggedUser;
    }
    @RequestMapping("/about")
    public String about()
    {
        return "about";
    }
    @RequestMapping("/contact")
    public String contact()
    {
        return "contact";
    }
    @RequestMapping("/logout")
    public String showLoggedout(){
        return "logout";
    }

}
