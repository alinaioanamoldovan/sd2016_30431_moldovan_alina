package com.proiect.controller;

import com.proiect.dao.BillDao;
import com.proiect.dao.TransactionDao;
import com.proiect.dao.UserDao;
import com.proiect.model.Bill;
import com.proiect.model.Report;
import com.proiect.model.Transactions;
import com.proiect.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Moldovan on 4/11/2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @RequestMapping
    public String admin() {
        return "admin";
    }

    @Autowired
    private UserDao userDao;

    @RequestMapping("/addUser")
    public String registerUser(Model model) {
        Users user = new Users();

        model.addAttribute("user", user);

        return "addUser";
    }


    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String registerCustomerPost(@Valid @ModelAttribute("user") Users user, BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "addUser";
        }

        List<Users> userList = userDao.getAllUsers();

        for (int i = 0; i < userList.size(); i++) {
            if (user.getEmailId().equals(userList.get(i).getEmailId())) {
                model.addAttribute("emailMsg", "Email already exists");

                return "addUser";
            }

            if (user.getUsername().equals(userList.get(i).getUsername())) {
                model.addAttribute("usernameMsg", "Username already exists");

                return "addUser";
            }
        }

        user.setEnabled(1);
        userDao.addUser(user);
        return "redirect:/admin/userList";
    }

    @RequestMapping("/userList/viewUsers/{userId}")
    public String viewAccount(@PathVariable int userId, Model model) throws IOException {
        Users user = userDao.getUserById(userId);
        model.addAttribute(user);

        return "viewUsers";


    }

    @RequestMapping("/userList")
    public String getAccount(Model model) {
        List<Users> users = userDao.getAllUsers();
        //Account account=accountList.get(0);
        model.addAttribute("users", users);
        return "userList";
    }

    @RequestMapping("/editUsers/{userId}")
    public String editAccount(@PathVariable int userId, Model model) {
        Users account = userDao.getUserById(userId);
        model.addAttribute(account);
        return "editUsers";
    }

    @RequestMapping(value = "/editUsers", method = RequestMethod.POST)
    public String editUserPost(@Valid @ModelAttribute("user") Users user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "editUsers";
        }

        userDao.editClient(user);
        return "redirect:/userList";
    }

    @Autowired
    private TransactionDao transactionDao;

    @RequestMapping("/deleteUser/{userId}")
    public String deleteClient(@PathVariable int userId, Model model) {

        Users account = userDao.getUserById(userId);
        //accountDao.deleteAccount(accountId);
        Bill bill = billDao.getBillByUsername(account.getUsername());
        Transactions tr = transactionDao.getTransactionByUsername(account.getUsername());
        userDao.deleteUser(userId);
        billDao.deleteBill(bill.getBillId());
        transactionDao.deleteTransaction(tr);
        return "redirect:/admin/userList";
    }

    @RequestMapping(value="/makeReport",method=RequestMethod.GET)
    public ModelAndView viewTransaction() throws IOException {
        //List<Transactions> transactions = transactionDao.getallTransactions(username);
        //model.addAttribute("transactions",transactions);
        ModelAndView model = new ModelAndView("report");
        List<Users> users = userDao.getAllUsers();
        List<Transactions> transactions = transactionDao.getAllTransaction();
        List<Bill> bill = billDao.getAllBill();
        //Account account=accountList.get(0);
        model.addObject("users", users);
        model.addObject("transactions", transactions);
        model.addObject("bill", bill);
        model.addObject("report",new Report());
        return model;


    }

    @Autowired
    public BillDao billDao;

    @RequestMapping(value = "/report")
    public String getReport(Model model) {
        List<Users> users = userDao.getAllUsers();
        List<Transactions> transactions = transactionDao.getAllTransaction();
        List<Bill> bill = billDao.getAllBill();
        //Account account=accountList.get(0);
        model.addAttribute("users", users);
        model.addAttribute("transactions", transactions);
        model.addAttribute("bill", bill);
        return "report_ugly";
    }
    @RequestMapping(value="/makeReport",method = RequestMethod.POST)
    public String addTransactions(@Valid @ModelAttribute("report") Report report, BindingResult result, ModelMap model)
    {
        if (result.hasErrors())
        {
            model.addAttribute("users", userDao.getAllUsers());
            model.addAttribute("transactions", transactionDao.getAllTransaction());
            model.addAttribute("bill", billDao.getAllBill());
            model.addAttribute("report",new Report());
        }
        String to = report.getStart();
        String from = report.getEnd();
        List<String> result1=new ArrayList<String>();
        for(Bill bill:billDao.getAllBill())
        {
            if (bill.getDate().contains(to) || bill.getDate().contains(from))
                //String result2= ;
                result1.add(bill.getUser()+bill.getAmount()+bill.getType()+bill.getAccount());
        }
        for(Transactions bill:transactionDao.getAllTransaction())
        {
            if (bill.getDate().contains(to) || bill.getDate().contains(from))
                //String result2= ;
                result1.add(bill.getUser()+bill.getAmount()+bill.getFromAccount()+bill.getToAccount());
        }
        model.addAttribute("resultList",result);
        return"report";
    }
}