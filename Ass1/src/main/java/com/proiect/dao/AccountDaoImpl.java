package com.proiect.dao;

import com.proiect.model.Account;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Moldovan on 4/10/2016.
 */
@Repository
@Transactional
public class AccountDaoImpl implements AccountDao {
  @Autowired
    private SessionFactory sessionFactory;

    public void addAccount(Account account) {
       // Client client = new Client();
       // account.setOwner(client);
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
        session.flush();

    }
    public void editAccount(Account account) {
        // account.setAccountId(1);
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
        session.flush();

    }

    public void updateBalance(String accountNum, double balance) {
        Session session = sessionFactory.getCurrentSession();
        Account account = (Account)session.get(Account.class,accountNum);
        account.setAmount(balance);

    }

    public Account getAccountById(int  accountId)
    {
           Session session=sessionFactory.getCurrentSession();
           Account account = (Account)session.get(Account.class,accountId);
           session.flush();
        return account;
    }



    public List<Account> getAllAccounts()
    {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Account");
        List<Account> account=query.list();
        session.flush();

        return account;
    }

    public Account getAccountByAccountNum(String accountNum) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Account a where a.accountNum=?");
        //List<Account> account=query.list();
        query.setParameter(0,accountNum);
        session.flush();
        return (Account)query.uniqueResult();

    }

    public void deleteAccount(int accountId)
    {
        Session session=sessionFactory.getCurrentSession();
        session.delete(getAccountById(accountId));
        session.flush();
    }
    public void updateAccount(Account account,double balance) {
        this.sessionFactory.getCurrentSession().merge(account);
    }
    public Account merge(Account detachedInstance) {
        //log.debug("merging Account instance");
        try {
            Account result = (Account) sessionFactory.getCurrentSession()
                    .merge(detachedInstance);
            //log.debug("merge successful");
            return result;
        } catch (RuntimeException re) {
            //log.error("merge failed", re);
            throw re;
        }
    }

    public Account getAccountByOwner(String owner) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Account a where a.owner=?");
        //List<Account> account=query.list();
        query.setParameter(0,owner);
        session.flush();
        return (Account)query.uniqueResult();

    }
}
