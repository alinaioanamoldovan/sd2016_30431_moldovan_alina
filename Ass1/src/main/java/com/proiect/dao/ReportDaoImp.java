package com.proiect.dao;

import com.proiect.model.Report;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Toshiba on 4/26/2016.
 */
@Repository
@Transactional
public class ReportDaoImp implements ReportDao {
     @Autowired
     SessionFactory sessionFactory;
    public void addReport(Report report) {
       Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(report);
        session.flush();
    }

    public List<Report> getAllReports() {
        //Session session = sessionFactory.getCurrentSession();
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Report");
        List<Report> client=query.list();
        session.flush();

        return client;

    }
}
