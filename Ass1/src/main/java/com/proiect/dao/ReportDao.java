package com.proiect.dao;

import com.proiect.model.Report;

import java.util.List;

/**
 * Created by Toshiba on 4/26/2016.
 */
public interface ReportDao {
    public void addReport(Report report);
    public List<Report> getAllReports();
}
