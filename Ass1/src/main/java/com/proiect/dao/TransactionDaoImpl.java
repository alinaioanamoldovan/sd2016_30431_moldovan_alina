package com.proiect.dao;

import com.proiect.model.Account;
import com.proiect.model.Transactions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Moldovan on 4/18/2016.
 */
@Repository
@Transactional
public class TransactionDaoImpl implements TransactionDao {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    AccountDao accountDao;

    public void addNewTransaction(Transactions transaction) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(transaction);
        session.flush();

    }


    public void deleteTransaction(Transactions transaction) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(transaction);
        session.flush();
    }

    public Transactions mergeTransaction(Transactions transaction) {

        Transactions result = (Transactions) sessionFactory.getCurrentSession().merge(transaction);
        return result;
    }

    public Transactions findById(int transactionId) {
        Transactions instance = (Transactions) sessionFactory.getCurrentSession().get(Transactions.class, transactionId);
        return instance;
    }

    public Transactions findByAccountNum(String accountNum) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Transactions t where t.fromAccount=? or t.toAccount=?");
        //List<Account> account=query.list();
        query.setParameter(0,accountNum);
        query.setParameter(1,accountNum);
        session.flush();
        return (Transactions)query.uniqueResult();

    }

    public Transactions getTransactionByUsername(String username) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Transactions t where t.user=?");
        query.setParameter(0,username);
        session.flush();
        return (Transactions)query.uniqueResult();
    }

    public List<Transactions> getAllTransaction() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("From Transactions");
        List<Transactions> transactions = query.list();
        return transactions;
    }
}

