package com.proiect.dao;

/**
 * Created by Moldovan on 4/18/2016.
 */
import com.proiect.model.*;

import java.util.List;

public interface TransactionDao {
    void addNewTransaction(Transactions transaction);
    void deleteTransaction(Transactions transaction);
    Transactions mergeTransaction(Transactions transaction);
    Transactions findById(int transactionId);
    Transactions findByAccountNum(String accountNum);
    public Transactions getTransactionByUsername(String username);
    List<Transactions> getAllTransaction();

}
