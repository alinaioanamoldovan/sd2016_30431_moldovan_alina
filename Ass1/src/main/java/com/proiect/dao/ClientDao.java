package com.proiect.dao;

import com.proiect.model.Client;

import java.util.List;

/**
 * Created by Moldovan on 4/12/2016.
 */
public interface ClientDao {
    void addClient(Client customer);

    Client getClientById(int clientId);

    List<Client> getAllClients();

    Client getClientByName(String name);
    void deleteClient(int clientId);
    void editClient(Client client);
    Client getClientByAccount(String accountNum);
}
