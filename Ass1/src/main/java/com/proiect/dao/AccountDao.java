package com.proiect.dao;

import com.proiect.model.Account;

import java.util.List;

/**
 * Created by Moldovan on 4/10/2016.
 */
public interface AccountDao {
    void addAccount(Account account);

    Account getAccountById(int accountId);
   //public void updateAccount(Account account);
    List<Account> getAllAccounts();
    Account getAccountByAccountNum(String accountNum);
    void deleteAccount(int accountId);
    void editAccount(Account account);
   // void updateAccount(String accountNum, double balance);
    Account merge(Account detachedInstance);
    public Account getAccountByOwner(String owner);
    void updateAccount(Account requestedBillAcount, double amount);
}
