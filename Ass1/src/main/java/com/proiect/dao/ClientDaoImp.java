package com.proiect.dao;


import com.proiect.model.Client;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Moldovan on 4/12/2016.
 */
@Repository
@Transactional
public class ClientDaoImp implements ClientDao
{
    @Autowired
    private SessionFactory sessionFactory;

    public void addClient(Client client)
    {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(client);
        session.flush();
    }
    public Client getClientById(int  clientId)
    {
        Session session=sessionFactory.getCurrentSession();
        Client client = (Client)session.get(Client.class,clientId);
        session.flush();
        return client;
    }
    public List<Client> getAllClients()
    {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Client");
        List<Client> client=query.list();
        session.flush();

        return client;
    }

    public Client getClientByName(String fname) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Client where fname = ?");
        query.setString(0,fname);


        return (Client) query.uniqueResult();
    }
    public void deleteClient(int clientId)
    {
        Session session=sessionFactory.getCurrentSession();
        session.delete(getClientById(clientId));
        session.flush();
    }
    public void editClient(Client client) {
        // account.setAccountId(1);
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(client);
        session.flush();

    }

    public Client getClientByAccount(String accountNum) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Client c where c.accountNum=?");
        query.setParameter(0,accountNum);
        session.flush();
        return (Client)query.uniqueResult();

    }
}

