package com.proiect.dao;

import com.proiect.model.Users;

import java.util.List;

/**
 * Created by Moldovan on 4/12/2016.
 */
public interface UserDao {
    void addUser(Users user);

    List<Users> getUsersByRole();
    Users getUserById(int userId);
    List<Users> getAllUsers();

    Users getUserByUsername(String username);
    void deleteUser(int userId);
    void editClient(Users user);
}
