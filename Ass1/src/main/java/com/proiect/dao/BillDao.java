package com.proiect.dao;

import com.proiect.model.Bill;

import java.util.List;

/**
 * Created by Toshiba on 4/23/2016.
 */
public interface BillDao {
    public void addBill(Bill bill);
    public Bill getBillById(int bill);
    public void deleteBill(int billId);
    public Bill getBillByAccountNum(String accountNum);
    public Bill getBillByUsername(String username);
    public List<Bill> getAllBill();
}
