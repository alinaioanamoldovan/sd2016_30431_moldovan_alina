package com.proiect.dao;

import com.proiect.model.Account;
import com.proiect.model.Bill;
import com.proiect.model.Users;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Toshiba on 4/23/2016.
 */
@Repository
@Transactional
public class BillDaoImpl implements BillDao {
    @Autowired
    SessionFactory sessionFactory;
    public void addBill(Bill bill) {
        Session session = sessionFactory.getCurrentSession();
        session.save(bill);
        session.flush();
    }
    public Bill getBillById(int  billId)
    {
        Session session=sessionFactory.getCurrentSession();
        Bill client = (Bill)session.get(Bill.class,billId);
        session.flush();
        return client;
    }
    public void deleteBill(int billId)
    {
        Session session=sessionFactory.getCurrentSession();
        session.delete(getBillById(billId));
        session.flush();
    }

    public Bill getBillByAccountNum(String accountNum) {

        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Bill b where b.account=?");
        //List<Account> account=query.list();
        query.setParameter(0,accountNum);
        session.flush();
        return (Bill)query.uniqueResult();

    }

    public Bill getBillByUsername(String username) {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Bill b where b.user=?");
        query.setParameter(0,username);
        session.flush();
        return (Bill)query.uniqueResult();
    }
    public List<Bill> getAllBill()
    {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Bill");
        List<Bill> client=query.list();
        session.flush();

        return client;
    }

}
