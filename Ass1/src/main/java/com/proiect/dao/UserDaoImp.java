package com.proiect.dao;

import com.proiect.model.Authorities;
import com.proiect.model.Users;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Moldovan on 4/12/2016.
 */
@Repository
@Transactional
public class UserDaoImp implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;
    public void addUser(Users user)
    {
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(user);
        Authorities auth = new Authorities();
        //auth.setAuthoritiesId(user.getUserId());
        auth.setUsername(user.getUsername());
        auth.setAuthority(user.getRole());
        session.saveOrUpdate(auth);

        session.flush();
    }


    public Users getUserById(int  userId)
    {
        Session session=sessionFactory.getCurrentSession();
        Users client = (Users)session.get(Users.class,userId);
        session.flush();
        return client;
    }

    public List<Users> getAllUsers()
    {
        Session session=sessionFactory.getCurrentSession();
        Query query;
        query = session.createQuery("From Users");
        List<Users> client=query.list();
        session.flush();

        return client;
    }
    public List<Users> getUsersByRole()
    {
        Session session1=sessionFactory.getCurrentSession();
        Query query= session1.createQuery("From Users where role=ROLE_USER");
        List<Users> users=query.list();
        session1.flush();

        return users;
    }
    public void deleteUser(int userId)
    {
        Session session=sessionFactory.getCurrentSession();
        session.delete(getUserById(userId));
        session.flush();
    }
    public void editClient(Users client) {
        // account.setAccountId(1);
        Session session=sessionFactory.getCurrentSession();
        session.saveOrUpdate(client);
        session.flush();

    }
    public Users getUserByUsername(String username)
    {
        Session session=sessionFactory.getCurrentSession();
        Query query=session.createQuery("From Users u where u.username=?");
        query.setParameter(0,username);
        session.flush();
        return (Users)query.uniqueResult();

    }
}
