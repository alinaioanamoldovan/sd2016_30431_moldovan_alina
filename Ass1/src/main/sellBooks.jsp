<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true" %>
<html xmlns:th="http://www.thymeleaf.org">
<style type="text/css">
body {
	bgcolor="	#808080"
}

ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
   
}

li {
    float: left;
}

li a {
    display: block;
    color: black;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

table {
    border-collapse: collapse;
}


</style>
<head>
<meta charset="ISO-8859-1">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sell Books</title>
</head>
<body>
	<ul>
	  <li><a href="http://localhost:8080/spring/homeEmployee">Home</a></li>
	  <li><a href="http://localhost:8080/spring/sellBooks">Sell books</a></li>
	  <li style="float:right"><a href="http://localhost:8080/spring/login">Logout</a></li>
	 
	</ul>
	
	
		<form name="employeeForm" method='POST'>
	
	
	<br><br>
	  <div align="center">
            <h1>Shopping Cart </h1>
            <table border="0">

                <th>No</th>
                <th>Title</th>
                <th>Author</th>
                <th>Price</th>
                 
                <c:forEach var="books" items="${booksList}" varStatus="status">
                <tr>
                   
                  
  					<td>${status.index + 1}</td>
                    <td>	
							<a href="<c:url value="http://localhost:8080/spring/bookDetailsEmp">  
			               <c:param name="code" value="${books.code}"/> 
			               
			               </c:url>" > <c:if test="${books.outOfStock == 'false'}">${books.title}</c:if>
			                <c:if test="${books.outOfStock == 'true'}">   <font color="red">${books.title}</font></c:if></a> 
			             
						
					</td>
                    <td>${books.author}</td>
                    <td>${books.price}</td>
                    <td>	
							<a href="<c:url value="http://localhost:8080/spring/removeFromCart">  
			               <c:param name="code" value="${books.code}"/> 
			               
			                </c:url>" >remove</a> 
						
					</td>
                             
                </tr>
                </c:forEach>    
                  <tr>
  					<td></td>
                    <td></td>
                    <td></td>
                    <td>${total}</td>
                    <td></td>
                             
                </tr>  
            <tr>
            <td><input name="submit" type="submit" value="sell" /></td>
           </tr>       
            </table>
         
        </div>
        
	</form>

</body>
</html>