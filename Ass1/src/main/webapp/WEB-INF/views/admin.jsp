<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Administrator page</h1>

            <p class="lead">This is the administrator page!</p>
        </div>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2>
                Welcome: ${pageContext.request.userPrincipal.name} |
                    <c:url var="logoutUrl" value="/logout"/>
                <form action="${logoutUrl}" method="post">
                    <input type="submit" value="Logout"/>
                </form>

        </c:if>
        <a href="<c:url value="/admin/addUser"/>">Add a new Employee</a>
            <a href="<c:url value="/admin/userList"/>">View users</a>
                <a href="<c:url value="/admin/makeReport"/>">Report</a>
                <a href="<c:url value="/admin/report"/>">Report_ugly</a>
    </div>
</div>


<%@ include file="/WEB-INF/views/template/footer.jsp" %>
