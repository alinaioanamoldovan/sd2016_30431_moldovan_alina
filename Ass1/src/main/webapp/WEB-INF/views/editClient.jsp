<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Edit  account</h1>
            <p class="lead" align="left">Fill the bellow information in order to edit the information of a client</p>
            <form:form action="${pageContext.request.contextPath}/employee/editClient" method="post" commandName="client" enctype="multipart/form-data">
                <form:hidden path="clientId" value="${client.clientId}"/>
            <div class="form-group">
                <label for="account number">First Name</label>
                <form:input path="fname" id="account number" class="form-Control" value="${client.fname}"></form:input>
            </div>
            <div class="form-group">
                <label for="account number">Last Name</label>
                <form:input path="lname" id="account number" class="form-Control" value="${client.lname}"></form:input>
            </div>

            <div class="form-group">
                <label for="account number">Date of birth</label>
                <form:input path="dob" id="account number" class="form-Control" value="${client.dob}"></form:input>
            </div>

            <div class="form-group">
                <label for="owner">CNP</label>
                <form:input path="CNP" id="owner" class="form-Control" value="${client.CNP}"></form:input>
            </div>
            <div class="form-group">
                <label for="balance">Address</label>
                <form:input path="address" id="balance" class="form-Control" value="${client.address}"></form:input>
            </div>
            <div class="form-group">
                <label for="date">City</label>
                <form:input path="city" id="date" class="form-Control" value="${client.city}"></form:input>
            </div>
            <div class="form-group">
                <label for="date">Zip code</label>
                <form:input path="zipcode" id="date" class="form-Control" value="${client.zipcode}"></form:input>
            </div>
            <div class="form-group">
                <label for="date">Phone number</label>
                <form:input path="phone" id="date" class="form-Control" value="${client.phone}"></form:input>
            </div>
            <div class="form-group">
                <label for="date">Email</label>
                <form:input path="emailId" id="date" class="form-Control" value="${client.emailId}"></form:input>
            </div>


            <br><br>

            <input type="submit" value="Submit" class="btn btn-success">
            <a href="<c:url value="/employee/clientList"/>">Cancel</a>
            </form:form>
            <%@include file="/WEB-INF/views/template/footer.jsp"%>






