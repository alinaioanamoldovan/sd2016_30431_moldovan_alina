<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Clients</h1>
            <p class="lead" align="left">Check out all the available clients</p>
            <table class="table table-striped table-hover " >
                <thead>
                <tr class="bg-success">
                    <th>Client first name</th>
                    <th>Client last name</th>
                    <th>Client phone number</th>

                    <th></th>

                </tr>
                </thead>
                <c:forEach items="${clients}" var="client">
                    <tr>
                        <td>${client.fname}</td>
                        <td>${client.lname}</td>
                        <td>${client.phone}</td>
                        <td><a href="<spring:url value="/employee/clientList/viewClient/${client.clientId}" />">
                            <span class="glyphicon glyphicon-info-sign"></span>
                        </a>
                            <a href="<spring:url value="/employee/deleteClient/${client.clientId}" />">
                                <span class="glyphicon glyphicon-remove-sign"></span>
                            </a>
                            <a href="<spring:url value="/employee/editClient/${client.clientId}" />">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                    </tr>
                </c:forEach>

            </table>

        </div>

        <%@include file="/WEB-INF/views/template/footer.jsp"%>






