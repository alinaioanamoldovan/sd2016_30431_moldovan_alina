<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Add client</h1>
            <p class="lead" align="left">Fill the bellow information in order to add a client</p>
            <form:form action="${pageContext.request.contextPath}/employee/addClient" method="post" commandName="client" enctype="multipart/form-data">

            <div class="form-group">
                <label for="name">First Name</label><form:errors path="fname" cssStyle="color:mediumpurple;"/>
                <form:input path="fname" id="name" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="name">Last Name</label><form:errors path="lname" cssStyle="color:mediumpurple;"/>
                <form:input path="lname" id="name" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="cnp">CNP</label><form:errors path="CNP" cssStyle="color:mediumpurple;"/>
                <form:input path="CNP" id="cnp" class="form-Control"></form:input>
                <span style="color: #ff0000">${cnpMsg}</span>
            </div>
            <div class="form-group">
                <label for="address">Address</label><form:errors path="address" cssStyle="color:mediumpurple;"/>
                <form:input path="address" id="address" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">City</label><form:errors path="city" cssStyle="color:mediumpurple;"/>
                <form:input path="city" id="city" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">Date of birth</label><form:errors path="dob" cssStyle="color:mediumpurple;"/>
                <form:input path="dob" id="city" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">Zip code</label><form:errors path="zipcode" cssStyle="color:mediumpurple;"/>
                <form:input path="zipcode" id="city" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">Phone number</label><form:errors path="phone" cssStyle="color:mediumpurple;"/>
                <form:input path="phone" id="city" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">Email</label><form:errors path="emailId" cssStyle="color:mediumpurple;"/>
                <form:input path="emailId" id="city" class="form-Control"></form:input>
            </div>
            <div class="form-group">
                <label for="city">Account id</label><form:errors path="accountNum" cssStyle="color:mediumpurple;"/>
                <form:input path="accountNum" id="city" class="form-Control"></form:input>
            </div>
            <br><br>

            <input type="submit" value="Submit" class="btn btn-success">
            <a href="<c:url value="/employee/clientList"/>">Cancel</a>

            </form:form>
            <%@include file="/WEB-INF/views/template/footer.jsp"%>






