<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Edit  account</h1>
            <p class="lead" align="left">Fill the bellow information in order to edit an account</p>
            <form:form action="${pageContext.request.contextPath}/employee/editAccount" method="post" commandName="account" enctype="multipart/form-data">
            <form:hidden path="accountId" value="${account.accountId}"/>
            <div class="form-group">
                <label for="account number">Account number</label>
                <form:input path="accountNum" id="account number" class="form-Control" value="${account.accountNum}"></form:input>
            </div>

            <div class="form-group">
                <label for="category">Account type</label>
                <label class="checkbox-inline"><form:radiobutton path="accountType" id="category" value="savings"/>Savings </label>
                <label class="checkbox-inline"><form:radiobutton path="accountType" id="category" value="spending"/>Spending </label>
            </div>
            <div class="form-group">
                <label for="owner">Account owner</label>
                <form:input path="owner" id="owner" class="form-Control" value="${account.owner}"></form:input>
            </div>
            <div class="form-group">
                <label for="balance">Balance</label>
                <form:input path="amount" id="balance" class="form-Control" value="${acccount.amount}"></form:input>
            </div>
            <div class="form-group">
                <label for="date">Open date</label>
                <form:input path="openDate" id="date" class="form-Control" value="${account.openDate}"></form:input>
            </div>


            <div class="form-group">
                <label class="control-label" for="accountImage">Upload Picture</label>
                <form:input id="accountImage" path="accountImage" type="file" class="form:input-large" />
            </div>
            <br><br>

            <input type="submit" value="Submit" class="btn btn-success">
            <a href="<c:url value="/employee/accountList"/>">Cancel</a>
            </form:form>
            <%@include file="/WEB-INF/views/template/footer.jsp"%>






