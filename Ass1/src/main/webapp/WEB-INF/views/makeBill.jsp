<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>
<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Process a bill </h1>
            <p class="lead" align="left">Fill the bellow information in order to process a bill</p>
            <form:form action="${pageContext.request.contextPath}/employee/makeBill" method="post" commandName="bill" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group">
                    <label for="account number">Id</label><form:errors path="billId" cssStyle="color:mediumpurple;"/>
                    <form:input path="billId" id="account number" class="form-Control" value="${bill.billId}" ></form:input>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label for="account number">Name</label><form:errors path="type" cssStyle="color:mediumpurple;"/>
                    <form:input path="type" id="account number" class="form-Control" ></form:input>
                </div>
            </div>

            <div class="form-group">
                <label for="account number">Data</label><form:errors path="date" cssStyle="color:mediumpurple;"/>
                <form:input path="date" id="account number" class="form-Control" ></form:input>
            </div>
            <div class="form-group">
                <label for="balance">Balance</label><form:errors path="amount" cssStyle="color:mediumpurple;"/>
                <form:input path="amount" id="balance" class="form-Control"></form:input>
                <span style="color: #ff0000">${amountMsg}</span>
            </div>


            <td>Account</td>
            <td><form:select path="account">
                <c:forEach var="account" items="${accounts}">
                    <form:option value="${account.accountNum}"> ${account.owner} - ${account.amount} RON</form:option>
                </c:forEach>
            </form:select></td>
            <div class="form-group">
                <label for="account number">User</label><form:errors path="user" cssStyle="color:mediumpurple;"/>
                <form:input path="user" id="account number" class="form-Control" value="${pageContext.request.userPrincipal.name}" ></form:input>
            </div>

            <span style="color: #ff0000">${Error}</span>
            <br/><br/>
            <input type="submit" value="Submit" class="btn btn-success">
            <a href="<c:url value="/employee/makeBill"/>">Cancel</a>

            </form:form>
<%@include file="/WEB-INF/views/template/footer.jsp"%>