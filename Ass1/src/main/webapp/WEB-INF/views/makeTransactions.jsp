<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>
<div class="container-wrapper">
<div id="container">
    <div class="page-header">
        <h1>Process a bill </h1>
        <p class="lead" align="left">Fill the bellow information in order to process a bill</p>
        <form:form action="${pageContext.request.contextPath}/employee/makeTransactions" method="post" commandName="transactions" enctype="multipart/form-data">
        <div class="form-group">
            <div class="form-group">
                <label for="account number">Name</label><form:errors path="type" cssStyle="color:mediumpurple;"/>
                <form:input path="type" id="account number" class="form-Control" ></form:input>
            </div>
        </div>
        <div class="form-group">
            <label for="account number">Username</label><form:errors path="username" cssStyle="color:mediumpurple;"/>
            <form:input path="username" id="account number" class="form-Control" value="${pageContext.request.userPrincipal.name}"></form:input>
        </div>
        <div class="form-group">
            <label for="account number">Data</label><form:errors path="date" cssStyle="color:mediumpurple;"/>
            <form:input path="date" id="account number" class="form-Control" ></form:input>
        </div>
        <div class="form-group">
            <label for="balance">Balance</label><form:errors path="amount" cssStyle="color:mediumpurple;"/>
            <form:input path="amount" id="balance" class="form-Control"></form:input>
            <span style="color: #ff0000">${amountMsg}</span>
        </div>


            <form:select path="accountId" id="accountId" items="${accounts}" itemValue="accountNum" itemLabel="AccountNum" />


        <br/><br/>
        <input type="submit" value="Submit" class="btn btn-success">
        <a href="<c:url value="/employee/makeTransactions"/>">Cancel</a>

        </form:form>
<%@include file="/WEB-INF/views/template/footer.jsp"%>