import com.proiect.dao.ClientDao;
import com.proiect.model.Client;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Toshiba on 4/27/2016.
 */


@Transactional
public class TestClient {
    @Autowired
    private ClientDao clientDao;
    @Test
    public  void testFind()
    {
        List<Client> client1= clientDao.getAllClients();
        Assert.assertEquals(null,client1.get(0));
    }

}
