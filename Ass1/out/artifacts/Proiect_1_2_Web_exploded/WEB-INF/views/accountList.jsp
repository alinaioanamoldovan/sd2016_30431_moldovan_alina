<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ include file="/WEB-INF/views/template/header.jsp" %>



<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>All Accounts</h1>

            <p class="lead">Checkout all available accounts</p>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr class="bg-success">
                <th>Proto Thumb</th>
                <th>Account owner</th>
                <th>Amount</th>

                <th></th>
            </tr>
            </thead>
            <c:forEach items="${accounts}" var="account">
                <tr>
                    <td><img src="<c:url value="/resources/images/${account.accountId}.png" />" alt="image" style="width:100%"/></td>
                    <td>${account.owner}</td>
                    <td>${account.amount}</td>
                    <td><a href="<spring:url value="/employee/accountList/viewAccount/${account.accountId}" />">
                        <span class="glyphicon glyphicon-info-sign"></span>
                    </a>
                        <a href="<spring:url value="/employee/deleteAccount/${account.accountId}" />">
                            <span class="glyphicon glyphicon-remove-sign"></span>
                        </a>
                        <a href="<spring:url value="/employee/editAccount/${account.accountId}" />">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

<%@ include file="/WEB-INF/views/template/footer.jsp" %>