<%@include file="/WEB-INF/views/template/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Detailed view</h1>
            <p class="lead" align="left">Here are the detailes</p>
          <div class="container">
              <div class="row">
                  <div class="col-md-5">
                      <img src="<c:url value="/resources/images/${account.accountId}.jpg"/>" alt="image" style="width:100%"/>
                  </div>
               <div class="col-md-5">
                   <h3>${account.owner}</h3>
                   <p><strong>Account Number : </strong>${account.accountNum}</p>
                   <p><strong>Account Type : </strong>${account.accountType}</p>
                   <p><strong>Account balance : </strong>${account.amount}</p>
                   <p><strong>Account open date : </strong>${account.openDate}</p>
               </div>

              </div>


          </div>

        </div>

        <%@include file="/WEB-INF/views/template/footer.jsp"%>






