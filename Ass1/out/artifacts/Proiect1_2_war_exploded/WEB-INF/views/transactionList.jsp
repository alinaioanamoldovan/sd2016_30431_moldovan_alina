<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Users</h1>
            <p class="lead" align="left">Check out all the available users</p>
            <table class="table table-striped table-hover " >
                <thead>
                <tr class="bg-success">
                    <th>Users first name</th>
                    <th>Users last name</th>
                    <th>Users phone number</th>

                    <th></th>

                </tr>
                </thead>
                <c:forEach items="${transactions}" var="transaction">
                    <tr>
                        <td>${transaction.type}</td>
                        <td>${transaction.amount}</td>
                        <td>${transaction.data}</td>

                    </tr>
                </c:forEach>

            </table>

        </div>

        <%@include file="/WEB-INF/views/template/footer.jsp"%>






