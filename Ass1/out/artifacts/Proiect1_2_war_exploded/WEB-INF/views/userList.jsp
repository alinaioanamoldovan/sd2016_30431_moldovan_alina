<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Users</h1>
            <p class="lead" align="left">Check out all the available users</p>
            <table class="table table-striped table-hover " >
                <thead>
                <tr class="bg-success">
                    <th>Users first name</th>
                    <th>Users last name</th>
                    <th>Users phone number</th>

                    <th></th>

                </tr>
                </thead>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.fname}</td>
                        <td>${user.lname}</td>
                        <td>${user.phoneno}</td>
                        <td><a href="<c:url value="/admin/userList/viewUsers/${user.userId}" />">
                          View info
                        </a>
                            <a href="<spring:url value="/admin/deleteUser/${user.userId}" />">
                               Delete user
                            </a>
                            <a href="<c:url value="/admin/editUsers/${user.userId}" />">
                               Edit
                            </a>
                            <a href="<c:url value="/admin/transactionList" />">
                                ViewTrans
                            </a>
                            <a href="<c:url value="/admin/viewTransaction/${user.username}" />">
                                View Transactions
                            </a>
                        </td>
                    </tr>
                </c:forEach>

            </table>

        </div>

        <%@include file="/WEB-INF/views/template/footer.jsp"%>






