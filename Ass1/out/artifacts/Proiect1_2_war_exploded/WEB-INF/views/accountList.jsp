<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
   <div id="container">
       <div class="page-header">
        <h1>Accounts</h1>
        <p class="lead" align="left">Check out all the available accounts</p>
     <table class="table table-striped table-hover " >
         <thead>
         <tr class="bg-success">
             <th>Account image</th>
             <th>Account number</th>
             <th>Owner</th>

             <th></th>

         </tr>
         </thead>
         <c:forEach items="${accounts}" var="account">
     <tr>
         <td><img src="<c:url value="/resources/images/${account.accountId}.jpg"/>" alt="image" style="width:100%"/></td>
         <td>${account.accountNum}</td>
         <td>${account.owner}</td>

         <td><a href="<spring:url value="/employee/accountList/viewAccount/${account.accountId}" />">
             <span class="glyphicon glyphicon-info-sign"></span>
         </a>
             <a href="<spring:url value="/employee/deleteAccount/${account.accountId}" />">
                 <span class="glyphicon glyphicon-remove-sign"></span>
             </a>
             <a href="<spring:url value="/employee/editAccount/${account.accountId}" />">
                 <span class="glyphicon glyphicon-pencil"></span>
             </a>
         </td>
     </tr>
         </c:forEach>

     </table>

    </div>

<%@include file="/WEB-INF/views/template/footer.jsp"%>






