<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/views/template/header.jsp"%>



<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->




<div class="container-wrapper">
    <div id="container">
        <div class="page-header">
            <h1>Add user</h1>
            <p class="lead" align="left">Fill the bellow information in order to add a user</p>
            <form:form action="${pageContext.request.contextPath}/employee/makeTransfer" method="post" commandName="transactions" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="owner">Amount</label>
                    <form:input path="amount" id="owner" class="form-Control"></form:input>
                </div>


                <form:select path="fromAccount" id="fromAccount" items="${account}" itemValue="accountNum" itemLabel="AccountNum" />
                <c:out value="${transactions.fromAccount}"></c:out>

            <input type="submit" value="Submit" class="btn btn-success">
            <a href="<c:url value="/employee/accountList"/>">Cancel</a>

            </form:form>

            </div>
            <%@include file="/WEB-INF/views/template/footer.jsp"%>






